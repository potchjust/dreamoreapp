package com.dreammore.dreammoreapp.View.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


import com.dreammore.dreammoreapp.Helpers.Config;
import com.dreammore.dreammoreapp.R;
import com.paypal.android.sdk.payments.PayPalConfiguration;

public class PayPalActivity extends AppCompatActivity {

    public static final int PAYPAL_REQUEST_CODE=7171;
    public static PayPalConfiguration configuration=new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Config.PAYPAL_CLIENT_ID);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_pal);
    }
}
