/**
 * TODO penser à regler la partie ulpload d'audio (Ah oui hein)
 */
package com.dreammore.dreammoreapp.View.Dialog;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadata;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UploadDialog extends DialogFragment {


    private Uri image_uri;
    private String audioName;
    private Toolbar toolbar;
    private ContentValues contentValues = new ContentValues();
    private RecyclerView uploadListView;
    private final int GALLERY_REQUEST_CODE = 200;
    private final int PICTURE_REQUEST_CODE = 201;
    private final int AUDIO_REQUEST_CODE = 202;
    private MaterialButton pickFromGallery;
    private MaterialCardView takePhoto;
    private MaterialButton recordAudio;
    private LinearLayoutManager linearLayoutManager;
    private String photoPath;
    private String soundPath;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog!=null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public static UploadDialog Display(FragmentManager fragmentManager) {
        UploadDialog uploadDialog = new UploadDialog();
        uploadDialog.show(fragmentManager, "Upload");
        return uploadDialog;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.alert_attachements, container, false);
        this.pickFromGallery = view.findViewById(R.id.btn_pick_gallery);
       // this.takePhoto = view.findViewById(R.id.takePicture);
        this.recordAudio = view.findViewById(R.id.btn_recordAudio);

        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setStyle(DialogFragment.STYLE_NORMAL,R.style.AppTheme_FullScreenDialog);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        /**
         * setb on touch listener
         * pour faire les trucs de tap and hold
         *
         */


        this.recordAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT>=23){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        checkAudioPermission();
                    }
                }else{
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startAudioRecording();
                    }
                }
            }
        });
        /* fin partie record de l'audio*/

        /*partie import d'image*/
        this.pickFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {
                    checkGalleryPermissions();
                } else {
                    pickFromGallery();
                }

            }
        });
        /* fin de la partie sur l'import de l'image depuis la galérie*/

        /*partie avec la prise de phoot*/

       /* this.takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//**
                 * ma logique
                 * si la version du sdk es t supérieur ou égale à la version d'android M alors on demande la permission
                 * dans le cas contraire on lance la fonctionnalité
                 *
                 *//*
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPhotoPermissions();
                } else {
                    takePicture();
                }


            }
        });*/
    }

    /**
     * import
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startAudioRecording() {
        Intent soundIntent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        startActivityForResult(soundIntent, AUDIO_REQUEST_CODE);
        dismiss();
    }


    public void pickFromGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        getActivity().startActivityForResult(gallery, GALLERY_REQUEST_CODE);
        dismiss();
    }

    private void takePicture() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        HomeActivity activity = (HomeActivity) getActivity();
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            File photo = null;
            try {
                photo = createPicture();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            if (photo != null) {
                Uri uri = FileProvider.getUriForFile(activity, "com.dreammore.fileprovider", photo);
                Log.d("UriCreated", "takePicture: " + uri);
                // Uri uri=Uri.fromFile(photo);
                activity.setImagePath(uri.getPath());
                activity.setImageUri(uri);
                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, activity.getImageUri());
                getActivity().startActivityForResult(intent, PICTURE_REQUEST_CODE);
                //getActivity().startActivityFromFragment(this,intent,PICTURE_REQUEST_CODE);
            }
        }

    }

    private File createPicture() throws IOException {

        String timestanp = new SimpleDateFormat("yyyMMdd_HHmmss").format(new Date());
        String imageFileName = "PNG_" + timestanp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".png",
                storageDir
        );
        photoPath = image.getAbsolutePath();
        return image;
    }

    private void checkGalleryPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY_REQUEST_CODE);
        } else {
            pickFromGallery();
        }
    }

    private void checkPhotoPermissions() {
        /**
         * si la permission n'a pas été accepté alors afficher une notification
         * lui disant d'accepter la notification
         */

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICTURE_REQUEST_CODE);
        } else {
            takePicture();
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void checkAudioPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE}, AUDIO_REQUEST_CODE);
        } else {
            startAudioRecording();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /**
         * si le code de retour est égale au code passé en parametre alors effectué l'action correspondane
         */
        switch (requestCode) {
            case PICTURE_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePicture();
                } else {
                    this.takePhoto.setEnabled(false);
                }
                break;
            case AUDIO_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startAudioRecording();
                    }
                }

                break;
            case GALLERY_REQUEST_CODE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    pickFromGallery();
                } else {
                    this.pickFromGallery.setEnabled(false);
                }
                break;
        }

    }
}
