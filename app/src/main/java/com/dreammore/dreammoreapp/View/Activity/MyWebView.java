package com.dreammore.dreammoreapp.View.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.SingleActivity;

import es.dmoral.toasty.Toasty;

public class MyWebView extends AppCompatActivity {

    private WebView webView;
    private static final int CLOSE_REQUEST_CODE=500;
    String identifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //WebView webView = new WebView(this);
        setContentView(R.layout.activity_web_view);
        Intent intent=getIntent();
        Uri uri=intent.getData();
        String url=intent.getStringExtra("url");
        Log.d("url", "onCreate: "+url);
        WebView webView=(WebView)findViewById(R.id.webView);
        webView.loadUrl(url);
        this.identifier=uri.getQueryParameter("identifier");

    }



}
