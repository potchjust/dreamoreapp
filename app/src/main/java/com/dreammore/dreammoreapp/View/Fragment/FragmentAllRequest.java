package com.dreammore.dreammoreapp.View.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Adapter.RequestListAdapter;
import com.dreammore.dreammoreapp.ViewModel.FragmentRequestViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class FragmentAllRequest extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private ImageView emptyStateImage;
    private LinearLayout emptyStateView;
    private RecyclerView requestList;
    private final int GALLERY_REQUEST_CODE = 200;
    private final int PICTURE_REQUEST_CODE = 201;
    private final int AUDIO_REQUEST_CODE = 202;
    private FloatingActionButton addRequest;
    private TextView uploadedFileCount;
    private AppClient appClient;
    private String TAG = "Debug FragmentRequest";
    private Session session;
    private RequestListAdapter requestListAdapter;
    private FragmentRequestViewModel requestViewModel;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isLoaded=false;


    private void initFragmentViewComponents(View view) {
        this.addRequest = (view).findViewById(R.id.addrequest);
        this.requestList = (view).findViewById(R.id.fragment_request_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
       this.emptyStateImage=(view).findViewById(R.id.emptyStateImage);
        requestList.setLayoutManager(linearLayoutManager);
        this.appClient = AppService.initService(AppClient.class);
        this.session = new Session(getContext());
        this.requestViewModel = ViewModelProviders.of(this).get(FragmentRequestViewModel.class);
        this.swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.requestSwipeLayout);
        this.swipeRefreshLayout.setOnRefreshListener(this);
        progressDialog=new ProgressDialog(getContext());
        this.emptyStateView=view.findViewById(R.id.emptyState);
        progressDialog.setTitle("Dreammore");
        progressDialog.setMessage("Merci de patienter pendant la récuperation de la liste de vos requêtes \n");
        progressDialog.setCancelable(false);
       progressDialog.show();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.all_request_fragment, container, false);
        initFragmentViewComponents(this.view);
        Log.d("loaded", "onCreateView: "+isLoaded);
        return this.view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (progressDialog.isShowing()){
            Log.d(TAG, "setUserVisibleHint: loadData");
            loadClientRequest(session.readInt("client_id"));
        }
    }

    private void loadClientRequest(int client_id){

        this.requestViewModel.getMyRequests(client_id).observe(getViewLifecycleOwner(), new Observer<List<Draft>>() {
            @Override
            public void onChanged(List<Draft> drafts) {
                progressDialog.dismiss();
                if (!drafts.isEmpty()) {
                    requestListAdapter = new RequestListAdapter(drafts, getContext());
                    requestList.setAdapter(requestListAdapter);
                    emptyStateView.setVisibility(View.INVISIBLE);
                    isLoaded=true;
                } else {
                    emptyStateView.setVisibility(View.VISIBLE);
                    requestList.setVisibility(View.INVISIBLE);

                }

            }
        });
    }

    @Override
    public void onRefresh() {
        Toast.makeText(getContext(),"En rafraichissement",Toast.LENGTH_LONG).show();
        loadClientRequest(session.readInt("client_id"));
        if (isLoaded){
            swipeRefreshLayout.setRefreshing(false);
        }

    }
}



