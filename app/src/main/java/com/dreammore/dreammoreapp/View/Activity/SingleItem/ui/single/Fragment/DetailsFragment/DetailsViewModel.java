package com.dreammore.dreammoreapp.View.Activity.SingleItem.ui.single.Fragment.DetailsFragment;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dreammore.dreammoreapp.Model.Details;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.ui.single.Fragment.SingleRepository;

public class DetailsViewModel extends ViewModel {

    private SingleRepository singleRepository;
    public DetailsViewModel() {
       this.singleRepository=SingleRepository.getInstance();
    }

    public MutableLiveData<Details> getDetails(int customer_id, int request_id)
    {
        return this.singleRepository.getRequestDetails(customer_id,request_id);
    }
}
