package com.dreammore.dreammoreapp.View.Dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Client.PayGateClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Helpers.Config;
import com.dreammore.dreammoreapp.Helpers.Utils;
import com.dreammore.dreammoreapp.Model.Retrofit.PaymentStatus;
import com.dreammore.dreammoreapp.Model.Retrofit.Payment;
import com.dreammore.dreammoreapp.Model.Retrofit.SubPayment;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.Repository.RequestRepository;
import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.dreammore.dreammoreapp.View.Activity.MobileTransactionActivity;
import com.dreammore.dreammoreapp.View.Activity.MyWebView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;

import es.dmoral.toasty.Toasty;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentDialog extends DialogFragment {
    private String url="https://paygateglobal.com/v1/page?token=38e4549c-6653-4ada-b2da-b1496729ab0f";
    private MaterialButton btn_card;
    private MaterialButton btn_mobile;
    private int draft_id;
    private int montant;
    private AppClient appClient;
    private TextInputEditText paid;
    private Payment payment;
    private Payment processPayment;
    private int payment_id;
    private int purchase_id;
    private String identifer;
    private int id_payment;
    private int subPayId;
    private int checkId;
    private RequestRepository requestRepository=RequestRepository.getInstance();
    private SubPayment subPayment;
    public static final int PAYPAL_REQUEST_CODE=7171;
    private static final int CLOSE_REQUEST_CODE=500;
    public static PayPalConfiguration configuration=new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Config.PAYPAL_CLIENT_ID);

    public int getDraft_id() {
        return draft_id;
    }

    public void setDraft_id(int draft_id) {
        this.draft_id = draft_id;
    }

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public  PaymentDialog(int draft_id,int montant,int payment_id,int purchase_id,String identifer){
        this.draft_id = draft_id;
        this.montant = montant;
        this.payment_id = payment_id;
        this.purchase_id = purchase_id;
        this.identifer = identifer;
    }

    private String formurl(int amout,String identifier){
        String url ="https://paygateglobal.com/v1/page?token=38e4549c-6653-4ada-b2da-b1496729ab0f&amount="+amout+"&description=payement_commande&identifier="+identifier+"";
        return  url;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.dialog_payment,container,false);
        initFragment(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.appClient= AppService.initService(AppClient.class);
        this.btn_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    proceedToPay(Integer.parseInt(paid.getText().toString()),purchase_id);
            }
        });

        this.btn_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payWithCard(view);
            }
        });


        /*check if chrome available*/
        Intent payService=new Intent(getContext(),PayPalService.class);
        payService.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,configuration);
        getActivity().startService(payService);
        Log.d("identifier",this.identifer);
        Log.d("payment_id","payment_id"+this.purchase_id);



    }

    private void initFragment(View view){
            this.btn_mobile=view.findViewById(R.id.btn_mobile);
            this.btn_card=view.findViewById(R.id.btn_card);
            this.paid=view.findViewById(R.id.toPay);
    }

    public PaymentDialog displayDialog(FragmentManager fragmentManager,int draft_id,int montant,int payment_id,int purchase_id,String identifer){
        PaymentDialog paymentDialog=new PaymentDialog(draft_id,montant,payment_id,purchase_id,identifer);
        paymentDialog.show(fragmentManager,"paymentDialog");
        return paymentDialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog=getDialog();
        if (dialog!=null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    private void proceedToPay(int paid,int purchase_id){
        this.appClient.proceedPayment(purchase_id, paid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) throws Exception {
                        Intent intent=new Intent(getContext(), MobileTransactionActivity.class);
                        intent.putExtra("draft_id",draft_id);
                        intent.putExtra("url",formurl(paid,identifer));
                        intent.putExtra("identifier",identifer);
                        intent.putExtra("subId",integer);
                        getActivity().startActivityForResult(intent,CLOSE_REQUEST_CODE);
                        dismiss();

                        Log.d("sub","int"+integer);
                    }
                });

    }
    private void payWithCard(View view){

        Toasty.info(getContext(),"fonctionnalitée en cours développement ",Toasty.LENGTH_LONG).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().stopService(new Intent(getContext(),PayPalService.class));
    }

}
