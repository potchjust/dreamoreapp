package com.dreammore.dreammoreapp.View.Activity.SingleItem;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.dreammore.dreammoreapp.R;

import java.util.ArrayList;
import java.util.List;

public class SinglePageAdapter extends FragmentPagerAdapter {

    private List<String> fragmentTitle = new ArrayList<>();
    private Context Adaptercontext;
    private List<Fragment> fragments = new ArrayList<>();

    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_3, R.string.tab_text_4};

    public SinglePageAdapter(@NonNull FragmentManager fm,Context context) {
        super(fm);
        Adaptercontext=context;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitle.get(position);
    }

    public void initFragment(Fragment fragment,String title){
        fragments.add(fragment);
        fragmentTitle.add(title);
    }
}
