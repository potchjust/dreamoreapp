package com.dreammore.dreammoreapp.View.Dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.dreammore.dreammoreapp.R;

public class dialog_sample extends DialogFragment {

    private ImageView imageView;
    private TextView message;
    private void initComponent(View view){
        imageView=view.findViewById(R.id.dialog_icon);
        message=view.findViewById(R.id.dialog_message);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.dialog_request_send,container,false);
       initComponent(view);
       return view;
    }


    public dialog_sample display(FragmentManager fragmentManager){
        dialog_sample sample=new dialog_sample();
        sample.show(fragmentManager,"dialog_sample");;
        return sample;
    }


}
