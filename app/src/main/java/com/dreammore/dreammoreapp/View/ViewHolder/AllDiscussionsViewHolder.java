package com.dreammore.dreammoreapp.View.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dreammore.dreammoreapp.R;

public class AllDiscussionsViewHolder extends RecyclerView.ViewHolder {
    private TextView messageContent;
    public AllDiscussionsViewHolder(@NonNull View itemView) {
        super(itemView);
        messageContent=itemView.findViewById(R.id.messageContent);
    }

    public TextView getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(TextView messageContent) {
        this.messageContent = messageContent;
    }
}
