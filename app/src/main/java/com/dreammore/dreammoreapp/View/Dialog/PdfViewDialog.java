package com.dreammore.dreammoreapp.View.Dialog;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;

import java.io.InputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PdfViewDialog extends DialogFragment {


    private  PDFView pdfView;
    private AppClient appClient;
    private InputStream inputStream;
    private ProgressBar progressBar;
    private String uri;
    private Toolbar pdfViewToolbar;

    public PdfViewDialog(String url){
        this.uri=url;
    }
    private void initDialogComponent(View view){
        pdfViewToolbar=view.findViewById(R.id.pdfViewLayoutToolbar);
        pdfView=(PDFView) view.findViewById(R.id.pdfView);

        progressBar=view.findViewById(R.id.showPdfFileProgress);
        pdfViewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    @Override
    public void onStart(){
        super.onStart();
        Dialog dialog=getDialog();
        if (dialog!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    /*function where actions wll be done*/
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      //  progressBar.setVisibility(View.VISIBLE);

        loadFile(getUri());

    }
    /*fin function where actions wll be done*/

    /*oncreate*/
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    /*oncreate*/

    /*fonction permettant d'initialiser la vue*/
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View view=inflater.inflate(R.layout.dialog_pdfview,container,false);

        initDialogComponent(view);
        return view;
    }
    /*fin fonction permettant d'initialiser la vue*/


    public  PdfViewDialog display(FragmentManager fragmentManager,String uri){
        PdfViewDialog dialog=new PdfViewDialog(uri);

        this.appClient=AppService.initService(AppClient.class);
        dialog.show(fragmentManager,"pdfViewDialog");
        return dialog;
    }


    private void loadFile(String url){

            AppClient appClient=AppService.initService(AppClient.class);
            appClient.downloadBill(url).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful() && progressBar.isShown()){
                        progressBar.setVisibility(View.GONE);
                        pdfView.fromStream(response.body().byteStream())
                                .onError(new OnErrorListener() {
                                    @Override
                                    public void onError(Throwable t) {
                                        Log.d("uri", "onError: "+inputStream);
                                        Log.d("Error", "onError: "+t.getMessage());
                                        t.printStackTrace();
                                    }
                                })
                                .onRender(new OnRenderListener() {
                                    @Override
                                    public void onInitiallyRendered(int nbPages, float pageWidth, float pageHeight) {
                                        Log.d("nombreP", "onInitiallyRendered: "+nbPages);
                                    }
                                })
                                .load();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("pdf", "onFailure: ");
                    t.printStackTrace();
                }
            });

    }
}
