package com.dreammore.dreammoreapp.View.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.ViewHolder.AllDiscussionsViewHolder;

import java.util.List;

public class AllDiscussionsAdapter extends RecyclerView.Adapter<AllDiscussionsViewHolder> {

    private List<Discussion>getDiscussions;
    private Context context;

    public AllDiscussionsAdapter(Context context,List<Discussion>discussionList){
        this.context=context;
        getDiscussions=discussionList;
    }
    @NonNull
    @Override
    public AllDiscussionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.single_discussion_item,parent,false);
        return new AllDiscussionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllDiscussionsViewHolder holder, int position) {
            Discussion discussion=getDiscussions.get(position);
            String discussionContent=discussion.getContent();
            if (discussionContent.length()>=50){
                holder.getMessageContent().setText(discussionContent.substring(0,20));
            }else{
                holder.getMessageContent().setText(discussionContent);
            }

    }

    @Override
    public int getItemCount() {
        return getDiscussions.size();
    }
}
