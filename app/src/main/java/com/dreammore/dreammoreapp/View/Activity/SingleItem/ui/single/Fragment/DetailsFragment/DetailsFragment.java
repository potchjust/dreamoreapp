package com.dreammore.dreammoreapp.View.Activity.SingleItem.ui.single.Fragment.DetailsFragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Client.PayGateClient;
import com.dreammore.dreammoreapp.API.Service.AppService;

import com.dreammore.dreammoreapp.Helpers.Listener.OnSingleActivityFragmentListener;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Helpers.Utils;
import com.dreammore.dreammoreapp.Model.Details;
import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.Model.Request;
import com.dreammore.dreammoreapp.Model.RequestResponse;
import com.dreammore.dreammoreapp.R;

import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.ChatFragment.ChatViewModel;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.SingleActivity;
import com.dreammore.dreammoreapp.View.Adapter.SliderAdapter;
import com.dreammore.dreammoreapp.View.Dialog.CancelRequestDialog;
import com.dreammore.dreammoreapp.View.Dialog.PaymentDialog;
import com.dreammore.dreammoreapp.View.Dialog.PdfViewDialog;
import com.dreammore.dreammoreapp.View.Dialog.ValidateRequestDialog;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsFragment extends Fragment {

    private LinearLayout linearLayout;
    private boolean showBillDownloadButton;
    private DetailsViewModel mViewModel;
    private ProgressBar progressBar;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout lnl_button;
    private Intent intent;
    private Session session;
    private TextView subject, quantity, status, emmit_date, deliver_mode;
    private EditText content;
    private Button btn_viewBil, btn_CancelBill, btn_ValidateBill, btn_PayBill;
    private ImageView status_icon;
    private ViewPager viewPager;
    private ProgressDialog progressDialog;
    private SliderAdapter sliderAdapter;
    private boolean isLoaded;
    private LinearLayout actionButton;
    private int payment_id;
    private AppClient appClient;
    private PayGateClient payGateClient;
    private SingleActivity singleActivity;
    private HomeActivity homeActivity;
    private TextView txt_pay_status;
    private ImageView ic_payment_status;
    private LiveData<Request> Lrequest;
    private Request request;
    private LinearLayout stateContainer;

    private ChatViewModel chatViewModel;
    private TextView requestStateTxt;


    private int id_request;//represente l'id devenant de l'adapteur
    private int request_id;//represente l'id venant de la notiifaction

    private int draft_id;
    private int purchase_id;
    private int montant;
    private String bill_url;

    private OnSingleActivityFragmentListener listener;

    private static DetailsFragment detailsFragmentInstance;

    public static DetailsFragment getInstance() {
        if (detailsFragmentInstance == null) {
            detailsFragmentInstance = new DetailsFragment();
        }
        return detailsFragmentInstance;

    }


    public DetailsFragment() {

    }

    public DetailsFragment(SingleActivity singleActivity) {

        this.singleActivity = singleActivity;
        this.homeActivity = new HomeActivity();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.single_request_fragment, container, false);
        initViewComponent(view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void initViewComponent(View view) {
        this.subject = view.findViewById(R.id.request_subject);
        this.quantity = view.findViewById(R.id.draft_quantity);
        this.status = view.findViewById(R.id.draft_status);
        this.emmit_date = view.findViewById(R.id.draft_date);
        this.deliver_mode = view.findViewById(R.id.draft_deliverMode);
        this.content = view.findViewById(R.id.draft_content);
        this.status_icon = view.findViewById(R.id.ic_requestStatus);
        this.btn_ValidateBill = view.findViewById(R.id.userValidateRequest);
        this.btn_CancelBill = view.findViewById(R.id.userCancelRequest);
        this.btn_PayBill = view.findViewById(R.id.btn_PayBill);

        this.btn_viewBil = view.findViewById(R.id.btn_viewBill);
        this.linearLayout = view.findViewById(R.id.txtStateContainer);
        this.requestStateTxt = view.findViewById(R.id.requestStateTxt);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Dreammore");
        progressDialog.setMessage("Merci de patienter pendant l'affichage du contenu detaillé de votre requête");
        progressDialog.setCancelable(false);
        progressDialog.show();
        intent = getActivity().getIntent();

        /* this.lnl_button=view.findViewById(R.id.buttonPlace);*/

        session = new Session(getContext());
        appClient = AppService.initService(AppClient.class);
        /*  progressBar=view.findViewById(R.id.idValidateProgress);*/
        refreshLayout = view.findViewById(R.id.requestSwipeLayout);
        actionButton = view.findViewById(R.id.actionButtonGroup);
        /*lnl_button.setVisibility(View.VISIBLE);*/

        this.payGateClient = AppService.initService2(PayGateClient.class);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.id_request = getActivity().getIntent().getIntExtra("request_id", 0);
        this.request_id = intent.getIntExtra("accept_id", 0);

        mViewModel = ViewModelProviders.of(this).get(DetailsViewModel.class);
        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        // TODO: Use the ViewModel
        if (Utils.checkInternet()) {
            loadData(singleActivity.getDraft_id());
        } else {
            //TODO show no conection dialog
            new AlertDialog.Builder(getContext())
                    .setTitle("Dreammore:Etat de la connexion")
                    .setMessage("Veuillez vérifier votre connexion internet")
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(getActivity(), SingleActivity.class));
                            getActivity().finish();
                        }
                    })
                    .show();
        }

        Log.d("draft_id", "onActivityCreated: " + singleActivity.getDraft_id());


        this.btn_viewBil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bill", "onClick: " + bill_url);
                PdfViewDialog pdfViewDialog = new PdfViewDialog(bill_url);
                pdfViewDialog.display(getFragmentManager(), bill_url);
            }
        });

        /*checker si un */
        btn_PayBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Information")
                        .setMessage("Vous avez la possibilité de régler la totalité ou en deux tranches")
                        .setPositiveButton("Compris", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                PaymentDialog paymentDialog = new PaymentDialog(singleActivity.getDraft_id(), montant, payment_id, purchase_id, makedentifier());
                                paymentDialog.displayDialog(getFragmentManager(), singleActivity.getDraft_id(), montant, payment_id, purchase_id, makedentifier());
                            }
                        }).create().show();

            }
        });


        this.btn_ValidateBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ValidateMyBill(view);
            }
        });

        this.btn_CancelBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                canceMyBill(view);
            }
        });


    }




    public void ValidateMyBill(View view) {

        /*call the request inside here*/
        //progressBar.setVisibility(View.VISIBLE);

        this.appClient.validateBill(singleActivity.getDraft_id()).enqueue(new Callback<Draft>() {
            @Override
            public void onResponse(Call<Draft> call, Response<Draft> response) {
                if (response.isSuccessful()) {
                    ValidateRequestDialog validateRequestDialog = new ValidateRequestDialog(draft_id, montant);
                    Log.d("requestStatut", "onResponse: " + response.body().getId());
                    validateRequestDialog.display(getFragmentManager());
                    //startActivity(new Intent(getContext(),HomeActivity.class));
                } else {
                    Log.d("requestStatut", "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Draft> call, Throwable t) {
                if (t instanceof IOException) {
                    Snackbar snackbar = Snackbar.make(view, "Erreur de connection", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            snackbar.dismiss();
                        }
                    });
                }
            }
        });


    }

    public void canceMyBill(View view) {

        CancelRequestDialog.CancelRequest(getFragmentManager(), singleActivity.getDraft_id());
    }


    private void loadData(int id) {

        if (progressDialog.isShowing()) {
            mViewModel.getDetails(session.readInt("client_id"), id).observe(getViewLifecycleOwner(), new Observer<Details>() {
                @Override
                public void onChanged(Details details) {
                    progressDialog.dismiss();
                    montant = details.getDraft().getTotal_price();
                    purchase_id = details.getDraft().getPurchase_id();
                    isLoaded = true;
                    subject.setText(details.getDraft().getDraft_subject());
                    chatViewModel.admin_id.setValue(details.getDraft().getAdminUserId());
                    singleActivity.setAdmin_id(details.getDraft().getAdmin_id());
                    singleActivity.getToolbar().setTitle(details.getDraft().getDraft_subject());
                    quantity.setText(String.valueOf(details.getDraft().getDraft_quantity()));
                    String[] date = details.getDraft().getReceived_at().split(" ");
                    emmit_date.setText(date[0]);
                    content.setText(details.getDraft().getDraftContent());
                    bill_url = details.getDraft().getUrl();
                    if (details.getDraft().getDeliverMode() == 1) {
                        deliver_mode.setText("Express");
                    } else {
                        deliver_mode.setText("Classique");
                    }

                    switch (details.getDraft().getStatut()) {
                        case -3:
                            status_icon.setImageResource(R.drawable.ic_error);
                            break;
                        case 4:
                            status_icon.setImageResource(R.drawable.ic_verified);
                            btn_viewBil.setVisibility(View.VISIBLE);
                            btn_PayBill.setVisibility(View.GONE);
                            actionButton.setVisibility(View.VISIBLE);
                            break;
                        case 5:
                            linearLayout.setVisibility(View.VISIBLE);
                            linearLayout.setBackgroundColor(getResources().getColor(R.color.successColor));
                            requestStateTxt.setText("Vous avez validé votre facture.vous recevrez une notification vous invitant à proceder au paiement");
                            status_icon.setImageResource(R.drawable.ic_verified);
                            btn_viewBil.setVisibility(View.VISIBLE);
                            btn_PayBill.setVisibility(View.GONE);
                            actionButton.setVisibility(View.GONE);
                            break;
                        case 6 :
                            status_icon.setImageResource(R.drawable.ic_verified);
                            requestStateTxt.setText("Veuillez proceder au paiement de votre facture.Ce paiement se fera en deux tranches ");
                            btn_viewBil.setVisibility(View.VISIBLE);
                            btn_PayBill.setVisibility(View.VISIBLE);
                            actionButton.setVisibility(View.GONE);
                            break;

                            case 7 :
                            status_icon.setImageResource(R.drawable.ic_pending);
                            requestStateTxt.setText("Requete en cours de paiement ");
                            linearLayout.setBackgroundColor(getResources().getColor(R.color.colorInfo ));
                            btn_viewBil.setVisibility(View.VISIBLE);
                            btn_PayBill.setVisibility(View.VISIBLE);
                            actionButton.setVisibility(View.GONE);
                            break;

                        default:
                            status_icon.setImageResource(R.drawable.ic_pending);
                            btn_viewBil.setVisibility(View.GONE);
                            actionButton.setVisibility(View.GONE);
                            break;

                    }


                }
            });
        }

    }

    @SuppressLint("NewApi")
    private String makedentifier() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String uuid = timestamp.getTime() + "-" + UUID.randomUUID().toString();
        return uuid;
    }



    /*    request_id*/


    private void updateUi() {
        Constraints constraints = new Constraints.Builder()
                .setRequiresDeviceIdle(true)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();


    }

    @Override
    public void onResume() {
        super.onResume();
        updateUi();
    }

}

