/**
 * TODO lorsque l'utilisateur entre ses informations d'abord on va vérifier dans la bd s'il y est
 * TODO déja Dans ce cas on le reconnecte en lui permettant de revoir ses données
 * TODO .Dans le cas contraire on continue vers le processus d'insctiption
 *
 * @<code>Skiel</code>
 */
package com.dreammore.dreammoreapp.View.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
/**
 * import des packages necessaire à l'initialisation d'accountKit
 */
import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Model.Auth;
import com.dreammore.dreammoreapp.Model.Customer;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Dialog.RegisterSucessDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterActivity extends AppCompatActivity {

    public final int REQUEST_CODE = 99;
    private TextInputEditText lname;
    private TextInputEditText fname;
    private TextInputEditText email;
    private TextInputEditText tel;
    private String client_fname, client_lname, client_email, client_tel,country_code;
    private Button finishSignup;
    private Toolbar toolbar;
    private AppClient appClient;
    private AlertDialog alertDialog;
    private Session session;
    private boolean existed = false;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initComponent();
        setSupportActionBar(this.toolbar);
        this.client_tel=getIntent().getStringExtra("phoneNumber");
        checkExistingUser(client_tel);
        /*
         * si tout est bon aller au menu principal
         */

        this.finishSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValues();
                registerUser(client_fname, client_lname, client_email, client_tel,country_code);
            }
        });


    }




    private void initComponent() {
        this.lname = findViewById(R.id.client_name);
        this.fname = findViewById(R.id.client_fname);
        this.email = findViewById(R.id.client_email);
        this.finishSignup = findViewById(R.id.btn_signUp);
        this.toolbar = findViewById(R.id.requestActivityToolbar);
        this.appClient = AppService.initService(AppClient.class);
        this.alertDialog = new AlertDialog.Builder(this).create();
        this.session = new Session(this);
        progressDialog = new ProgressDialog(this);

    }

    /*
     * recupérer les valeurs des champs*/
    private void getValues() {
        this.client_lname = this.lname.getText().toString();
        this.client_fname = this.fname.getText().toString();
        this.client_email = this.email.getText().toString();

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    /*
     * methode permettant de vérifier si'il y'a un autre user en fontion du numéro de tel tapé
     *
     * @param client_tel

     */

    public void checkExistingUser(String client_tel) {

        ProgressDialog  progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Dreammore ");
        progressDialog.setMessage("Veuillez patienter ! Nous verifions si vous avez déja un compte avec nous ");
        progressDialog.show();
        if (progressDialog.isShowing()) {
            appClient.verify(client_tel).enqueue(new Callback<Customer>() {
                @Override
                public void onResponse(Call<Customer> call, Response<Customer> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getCode() != 404) {
                                existed = true;
                                session.save("client_id", +response.body().getId());
                                session.save("client_user_id",+response.body().getUserId());
                                progressDialog.dismiss();
                                startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
                            } else {

                                existed = false;
                                progressDialog.dismiss();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<Customer> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }

    }

   /*
     * @param client_fname le nom du client
     * @param client_lname son prenom
     * @param client_email son email
     * @param client_tel   son numero de telephone*/

    public void registerUser(String client_fname, String client_lname, String client_email, String client_tel,String country_code) {

        appClient.register_client(client_fname, client_lname, client_email, client_tel,country_code).enqueue(new Callback<Auth>() {
            @Override
            public void onResponse(Call<Auth> call, Response<Auth> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getCode() == 200) {
                            session.save("client_id", response.body().getClient_id());
                            RegisterSucessDialog.display(getSupportFragmentManager());
                            new RegisterActivity().finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Auth> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
/*
 * si l'utilisateur existe déja on le connecte automatiquement
 * dans le cas contraire on continue le processus d'enregistrement
 * */