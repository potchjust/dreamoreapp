package com.dreammore.dreammoreapp.View.Dialog;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.dreammore.dreammoreapp.View.Activity.RegisterActivity;
import com.google.android.material.button.MaterialButton;

public class RegisterSucessDialog extends DialogFragment {

    private Button goToButton;

    private void initViewComponents(View view) {
        goToButton = view.findViewById(R.id.btn_continue);
    }


    @Override
    public void onStart(){
        super.onStart();
        Dialog dialog=getDialog();
        if (dialog!=null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    /**
     * bind le layout au dialog
     * Contient l'initialisation des Widgets
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return View
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_register_sucess, container, false);
        initViewComponents(view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        goToButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), HomeActivity.class));
                new RegisterActivity().finish();
                dismiss();

            }
        });
    }

    /**
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public static RegisterSucessDialog display(FragmentManager fragmentManager) {
        RegisterSucessDialog registerSucessDialog = new RegisterSucessDialog();
        registerSucessDialog.show(fragmentManager, "SuccesDialogDisplay");
        return registerSucessDialog;
    }


}
