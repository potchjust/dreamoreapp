package com.dreammore.dreammoreapp.View.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.Model.Request;
import com.dreammore.dreammoreapp.R;

import com.dreammore.dreammoreapp.View.Activity.SingleItem.SingleActivity;
import com.dreammore.dreammoreapp.View.ViewHolder.LastRequestVH;

import java.util.List;

public class LastRequestAdapter extends RecyclerView.Adapter<LastRequestVH> {


    private Context context;
    private List<Draft> draftList;


    public LastRequestAdapter(Context context, List<Draft> draftList) {
        this.context = context;
        this.draftList = draftList;

    }

    @NonNull
    @Override
    public LastRequestVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_request_row_v2, parent, false);
        return new LastRequestVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LastRequestVH holder, int position) {
        Draft draft = draftList.get(position);

        Log.d("inOn", "onBindViewHolder: " + draft.getDraft_subject());
        holder.getRequest_subject().setText(draft.getDraft_subject());

        switch (draft.getStatut()) {
            case 5:
                holder.getStatus_icon().setImageResource(R.drawable.ic_verified);
                break;
            case 6:
                holder.getStatus_icon().setImageResource(R.drawable.ic_verified);
                break;
            case -5:
                holder.getStatus_icon().setImageResource(R.drawable.ic_error);
                break;
            default:
                holder.getStatus_icon().setImageResource(R.drawable.ic_pending);
                break;

        }
        /*quand on clique sur le holder redirige vers une activity*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SingleActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("draft_id", draft.getId());
                intent.putExtra("admin_id", draft.getAdmin_id());
                intent.putExtra("draft_state", draft.getStatut());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return draftList.size();
    }


}
