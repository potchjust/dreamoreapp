package com.dreammore.dreammoreapp.View.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.dreammore.dreammoreapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import java.util.concurrent.TimeUnit;

public class OtpSendActivity extends AppCompatActivity {


    private CountryCodePicker countryCodePicker;
    private ProgressDialog progressDialog;
    private TextInputEditText numberHolder;
    private MaterialButton receiveCode;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String _phoneNumber;
    private FirebaseAuth auth =FirebaseAuth.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_send);
        initComponent();


    }
    private void initComponent(){
        this.countryCodePicker=findViewById(R.id.countryCode);
        this.numberHolder=findViewById(R.id.phoneNumber);
        this.receiveCode=findViewById(R.id.receiveCode);
        this.progressDialog = new ProgressDialog(this);

       this.receiveCode.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String phoneNumber=countryCodePicker.getSelectedCountryCodeWithPlus()+numberHolder.getText().toString();
               _phoneNumber=countryCodePicker.getSelectedCountryCode()+numberHolder.getText().toString();
                LoginPhone(phoneNumber,_phoneNumber);

           }
       });

    }

    private void LoginPhone(String phone,String secondNumber){
        progressDialog.setTitle("Dreammore");
        progressDialog.setMessage("Veuillez patienter pendant la vérification de votre numéro de téléphone");
        progressDialog.show();

        PhoneAuthProvider.getInstance().verifyPhoneNumber(phone, 60, TimeUnit.SECONDS, this, new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                progressDialog.dismiss();
                loginWithCredential(phoneAuthCredential,secondNumber);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                e.printStackTrace();
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                progressDialog.dismiss();
                super.onCodeSent(s, forceResendingToken);
                Intent intent=new Intent(OtpSendActivity.this,OtpReceiveActivity.class);
                intent.putExtra("verificationId",s);
                intent.putExtra("phoneNumber",secondNumber);
                startActivity(intent);
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(@NonNull String s) {
                super.onCodeAutoRetrievalTimeOut(s);
            }
        });

    }


    private  void loginWithCredential(PhoneAuthCredential phoneAuthCredential,String phoneNumber){
        auth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful() && task.getResult().getUser()!=null){
                                Log.d("duser",task.getResult().getUser().getPhoneNumber());
                                FirebaseUser user = task.getResult().getUser();
                                Intent intent = new Intent(OtpSendActivity.this,RegisterActivity.class);
                                intent.putExtra("phoneNumber",phoneNumber);
                                startActivity(intent);
                            }
                    }
                });

    }

}
