package com.dreammore.dreammoreapp.View.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Adapter.AllDiscussionsAdapter;
import com.dreammore.dreammoreapp.ViewModel.DiscussionViewModel;

import java.util.List;


public class FragmentNotification extends Fragment {


    private AlertDialog successDialog;
    private DiscussionViewModel discussionViewModel;
    private AlertDialog uploadDialog;
    private RecyclerView alldiscussionList;
    private boolean isLoadedOnce=false;
    private Session session;

    private View requestView;

    public FragmentNotification() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.requestView = inflater.inflate(R.layout.request_notifications, container, false);
        initVariable();
        initView(requestView);
        return requestView;
    }

    private void initView(View view) {
        alldiscussionList=view.findViewById(R.id.alldiscussionList);
    }

    private void initVariable(){
        this.discussionViewModel= ViewModelProviders.of(this).get(DiscussionViewModel.class);
        session=new Session(getContext());
    }

    private void clearFields() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("NotificationLoad", "onActivityCreated: Notification Load ");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()){

            if (isVisibleToUser && !isLoadedOnce){
                Log.d("Notification", "setUserVisibleHint: humFirst");
                isLoadedOnce=true;
            }
        }
    }

    private void LoadData(int client_id){
        this.discussionViewModel.getDiscussion(client_id).observe(getViewLifecycleOwner(), new Observer<List<Discussion>>() {
            @Override
            public void onChanged(List<Discussion> discussionList) {
                if (!discussionList.isEmpty()){
                    AllDiscussionsAdapter allDiscussionsAdapter=new AllDiscussionsAdapter(getContext(),discussionList);
                    alldiscussionList.setAdapter(allDiscussionsAdapter);
                    alldiscussionList.setLayoutManager(new LinearLayoutManager(getContext()));
                   // Toast.makeText(getContext(),"notEmpty",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getContext(),"Empty",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // LoadData(session.readInt("client_id"));
    }
}
