package com.dreammore.dreammoreapp.View.Activity.SingleItem.ui.single.Fragment;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Model.Details;
import com.dreammore.dreammoreapp.Model.Retrofit.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleRepository {

    private static SingleRepository singleRepository;
    private AppClient appClient;
    private Context context;
    private MutableLiveData<List<Details>> details;
    private boolean error;
    private MutableLiveData<Boolean> isSuccessFull;
    private MutableLiveData<Result>messageResult;


    public static SingleRepository getInstance() {
        if (singleRepository == null) {
            singleRepository = new SingleRepository();
        }
        return singleRepository;
    }

    public SingleRepository() {
        this.appClient = AppService.initService(AppClient.class);
        error = false;
        this.details = new MutableLiveData<>();
    }

    public MutableLiveData<Details> getRequestDetails(int client_id, int request_id) {
        MutableLiveData<Details> details = new MutableLiveData<>();
        this.appClient.getRequestDetails(client_id, request_id).enqueue(new Callback<Details>() {
            @Override
            public void onResponse(Call<Details> call, Response<Details> response) {
                if (response.isSuccessful()) {
                    Log.d("responseDataHeader", "onResponse: " + response.headers());
                    details.postValue(response.body());
                } else {
                    Log.d("responseData", "onResponse: " + response.message());
                    details.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<Details> call, Throwable t) {
                //en cas d'erreur
                t.printStackTrace();
            }
        });
        return details;
    }

    public boolean isError() {
        return error;
    }

    public MutableLiveData<Result> respondeTo(int customer_id, int request_id, int admin_id, String content) {

      messageResult=new MutableLiveData<>();
        this.appClient.RespondeToMessage(customer_id, request_id, admin_id, content).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Log.d("code", "onResponse: "+response.code());
                if (response.code()==200){
                    messageResult.postValue(response.body());
                }else {
                    messageResult.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return messageResult;
    }
}
