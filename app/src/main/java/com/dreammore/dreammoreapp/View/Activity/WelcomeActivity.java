package com.dreammore.dreammoreapp.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.R;

public class WelcomeActivity extends AppCompatActivity {


    private Session session;
    private Button button;


    private void initActivityComponents() {
        this.session = new Session(this);
        button = findViewById(R.id.btn_welcome);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initActivityComponents();
        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WelcomeActivity.this, RegisterActivity.class));
            }
        });
    }

}
