package com.dreammore.dreammoreapp.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import com.dreammore.dreammoreapp.R;
import com.google.android.material.button.MaterialButton;

public class RequestActivity extends AppCompatActivity {

    private MaterialButton uploadButton;
    private Toolbar toolbar;

    private void initViewComponent() {
        this.uploadButton = findViewById(R.id.uploadButton);
        this.toolbar = findViewById(R.id.requestActivityToolbar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_request_form);
        initViewComponent();
        setSupportActionBar(this.toolbar);
        this.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(RequestActivity.this, "Ho", Toast.LENGTH_SHORT).show();
            }
        });

    }


}
