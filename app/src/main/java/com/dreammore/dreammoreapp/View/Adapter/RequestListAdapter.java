package com.dreammore.dreammoreapp.View.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.SingleActivity;

import java.util.List;

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.RequestListViewHolder> {

    private List<Draft> draftList;

    private Context context;
    private String TAG = "ListTag";
    private Session session;

    public RequestListAdapter(List<Draft> draftList, Context context) {
        this.draftList = draftList;
        this.context = context;
        this.session = new Session(context);
    }


    @NonNull
    @Override
    public RequestListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_request_row, parent, false);
        return new RequestListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestListViewHolder holder, int position) {
        Draft draft = draftList.get(position);


        holder.request_status.setText("Traitement en cours");
        holder.request_subject.setText(draft.getDraft_subject());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.save("draft_id", draft.getId());
                Log.d(TAG, "onClick: "+draft.getId());
                Intent sinle_intent=new Intent(context,SingleActivity.class);
                sinle_intent.putExtra("request_id",draft.getId());
                context.startActivity(sinle_intent);
            }
        });
        /**
         * appele la vue single activity
         */
    }

    @Override
    public int getItemCount() {
        return draftList.size();
    }

    public class RequestListViewHolder extends RecyclerView.ViewHolder {

        public TextView request_subject, request_status;


        public RequestListViewHolder(@NonNull View itemView) {
            super(itemView);
            request_subject = (itemView).findViewById(R.id.request_row_subject);
            request_status = (itemView).findViewById(R.id.request_row_status);

        }
    }

}
