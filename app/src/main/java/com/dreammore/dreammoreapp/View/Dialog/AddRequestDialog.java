package com.dreammore.dreammoreapp.View.Dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;

import java.io.File;


import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Model.Attachments;
import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.Model.Request;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.Repository.*;


import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.dreammore.dreammoreapp.ViewModel.FragmentRequestViewModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.tapadoo.alerter.Alerter;


import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRequestDialog extends DialogFragment {

    private static String TAG = "RequestDialog";
    private Toolbar toolbar;
    private Session session;
    private AppClient appClient;
    private TextInputEditText subject, quantity, description, address;
    private Button goUpload, SendDraft;
    private RadioButton express, classique;
    private static ContentResolver contentResolver;
    private FragmentRequestViewModel fragmentRequestViewModel;
    private Draft draft;
    private ProgressDialog progressDialog;


    private int checkValue = 0;

    private TextView uploadedFileCount;

    private HomeActivity homeActivity;

    private String draft_subject, draft_content, draft_address;
    private int draft_quantity, attachements_count, deliver_mode;
    public List<MultipartBody.Part> files;
    public  RequestRepository requestRepository;

    /*instanciation des éléments*/


    private void initComponents(View view) {
        this.toolbar = view.findViewById(R.id.requestActivityToolbar);
        this.subject = view.findViewById(R.id.request_subject);
        this.quantity = view.findViewById(R.id.request_quantity);
        this.description = view.findViewById(R.id.request_content);
        this.express = view.findViewById(R.id.chk_request_express);
        this.classique = view.findViewById(R.id.chk_request_classique);
        this.goUpload = view.findViewById(R.id.uploadButton);
        this.SendDraft = view.findViewById(R.id.sendDraft);
        this.uploadedFileCount = view.findViewById(R.id.txt_uploadFileCount);
        this.appClient = AppService.initService(AppClient.class);
        this.session = new Session(getContext());
        contentResolver = getContext().getContentResolver();
        this.fragmentRequestViewModel = ViewModelProviders.of(this).get(FragmentRequestViewModel.class);
        progressDialog = new ProgressDialog(getContext());
        this.requestRepository=RequestRepository.getInstance();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_request_form, container, false);
        initComponents(view);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private MultipartBody.Part prepareFiles(Uri uri) {

        File file = new File(String.valueOf(uri));
        RequestBody attachment = RequestBody.create(MediaType.parse(contentResolver.getType(uri)), file);
        String _fileName = file.getName().replaceAll("\\s", "");
        String fileName = Normalizer.normalize(_fileName, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        Log.d("fileName", "prepareFiles: " + fileName);
        return MultipartBody.Part.createFormData("attachment[]", fileName, attachment);


    }

    private String getPath(Uri uri) {
        String result;
        //  String projection[] = {MediaStore.Images.Media.DATA};

        Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(column_index);
            cursor.close();
        }
        return result;
    }

    /*fonction d'upload de fichier*/



    private void uploadFile(Uri uri,int id_draft){
        RequestBody draft_id = RequestBody.create(MultipartBody.FORM, String.valueOf(id_draft));
        File file=new File(getPath(uri));
        RequestBody requestedFile= RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body=MultipartBody.Part.createFormData("attachment",file.getName(),requestedFile);
        appClient.sendDraftAttachments(draft_id,body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("acceptError", "accept: "+throwable.getMessage());
                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        AlertDialog dialog = new AlertDialog.Builder(getContext())
                                .setTitle("Dreammore")
                                .setMessage("Votre requête a bien été soumise . Nous vous reviendrons sous peu")
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        HomeActivity.getUploadedFileList().clear();
                                        startActivity(new Intent(getContext(), HomeActivity.class));
                                    }
                                }).create();
                        dialog.show();
                    }
                })
                .subscribe();

    }

    /*fonction d'upload de fichier*/

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                HomeActivity.getUploadedFileList().clear();
            }
        });


        /*ouverture de l'intent des fichiers à uploader*/
        this.goUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UploadDialog.Display(getFragmentManager());
            }
        });

        /*methode d'envoie des fichiers sur le serveur*/;

        this.SendDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setTitle("Dreammore");
                progressDialog.setMessage("Soumission de votre requête en cours");
                progressDialog.show();
                getValues();
                if (HomeActivity.getUploadedFileList().size() == 0) {
                    fragmentRequestViewModel.sendRequest(session.readInt("client_id"), draft_subject, draft_quantity, draft_content, deliver_mode).observe(getViewLifecycleOwner(), new Observer<Draft>() {
                        @Override
                        public void onChanged(Draft draft) {
                            progressDialog.dismiss();
                            Log.d("changed", "onChanged: "+draft.getId());
                            AlertDialog dialog=new AlertDialog.Builder(getContext())
                                    .setTitle("Dreammore")
                                    .setMessage("Votre requête a bien été soumise , Nous vous reviendrons sous peu")
                                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            HomeActivity.getUploadedFileList().clear();
                                            HomeActivity activity = new HomeActivity();
                                            startActivity(new Intent(getContext(), HomeActivity.class));
                                        }
                                    }).create();
                            dialog.show();
                        }
                    });
                }else {
                    appClient.sendRequest(session.readInt("client_id"), draft_subject, draft_quantity, draft_content, deliver_mode)
                            .flatMap(new Function<Draft, ObservableSource<?>>() {
                                @Override
                                public ObservableSource<?> apply(Draft draft) throws Exception {
                                    if (draft!=null){
                                        for (Uri uri:HomeActivity.getUploadedFileList()) {
                                            uploadFile(uri,draft.getId());
                                        }
                                        return Observable.just(draft);
                                    }
                                    return null;
                                }
                            })
                            .doOnComplete(new Action() {
                                @Override
                                public void run() throws Exception {
                                    AlertDialog dialog=new AlertDialog.Builder(getContext())
                                            .setTitle("Dreammore")
                                            .setMessage("Votre requête a bien été soumise , Nous vous reviendrons sous peu")
                                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    HomeActivity.getUploadedFileList().clear();
                                                    HomeActivity activity = new HomeActivity();
                                                    startActivity(new Intent(getContext(), HomeActivity.class));
                                                }
                                            }).create();
                                    dialog.show();
                                }
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new DisposableObserver<Object>() {
                                @Override
                                public void onNext(Object o) {
                                    Log.d("sub", "onNext: ");
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d("error", "accept: "+e.getMessage());
                                }

                                @Override
                                public void onComplete() {
                                    Log.d("sub", "onComplete: ");
                                }
                            });

                }
            }
        });

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity = new HomeActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);

        }
    }

    public static AddRequestDialog display(FragmentManager fragmentManager) {
        AddRequestDialog addRequestDialog = new AddRequestDialog();
        addRequestDialog.show(fragmentManager, "RequestAlert");
        return addRequestDialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        uploadedFileCount.setText(String.valueOf(HomeActivity.UploadFileCount()));

    }

    private void getValues() {
        this.draft_subject = this.subject.getText().toString();
        if (this.quantity.getText().toString().isEmpty() || this.quantity.getText().toString().equals(" ")) {
            this.draft_quantity = 0;
        } else {
            this.draft_quantity = Integer.parseInt(this.quantity.getText().toString());
        }
        this.draft_content = this.description.getText().toString();
        if (this.express.isChecked()) {
            this.deliver_mode = 1;
            this.classique.setChecked(false);
        } else if (this.classique.isChecked()) {
            this.deliver_mode = 2;
            this.express.setChecked(false);
        }
    }


}
