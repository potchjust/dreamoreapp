package com.dreammore.dreammoreapp.View.Activity.SingleItem.ui.single.Fragment.OrderFragment;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dreammore.dreammoreapp.R;

public class Order extends Fragment {

    private OrderViewModel mViewModel;

    public static Order newInstance() {
        return new Order();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View orderView=inflater.inflate(R.layout.order_fragment, container, false);
        return orderView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
    }

}
