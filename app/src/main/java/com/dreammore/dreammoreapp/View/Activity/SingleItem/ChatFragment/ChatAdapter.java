package com.dreammore.dreammoreapp.View.Activity.SingleItem.ChatFragment;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.dreammore.dreammoreapp.R;

import java.util.List;


public class ChatAdapter extends RecyclerView.Adapter {

    private List<Discussion>discussionList;
    private AppClient appClient;
    private Context context;
    private Session session;
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private RecyclerView.ViewHolder holder;
    private int position;


    public ChatAdapter(Context context){
        this.context=context;
    }
    public ChatAdapter(List<Discussion> discussions, Context context) {
        this.context=context;
        this.discussionList=discussions;
        this.session=new Session(context);
        this.appClient= AppService.initService(AppClient.class);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType==VIEW_TYPE_MESSAGE_RECEIVED){
            Log.d("Viewtype", "onCreateViewHolder: "+VIEW_TYPE_MESSAGE_RECEIVED);
            view=LayoutInflater.from(context).inflate(R.layout.item_discussion_receive,parent,false);
          return new MessageReceive(view);
        }else if (viewType==VIEW_TYPE_MESSAGE_SENT){
            Log.d("Viewtype", "onCreateViewHolder: "+VIEW_TYPE_MESSAGE_SENT);
            view=LayoutInflater.from(context).inflate(R.layout.item_discussion_send,parent,false);
            return  new MessageSend(view);
        }
        return  null;
    }

    @Override
    public int getItemViewType(int position) {
        Discussion discussion=discussionList.get(position);
        Log.d("Viewtype", "onCreateViewHolder: "+discussion.getFrom());
        if (discussion.getFrom()==0){
            return VIEW_TYPE_MESSAGE_SENT;
        }else{
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Discussion discussion=discussionList.get(position);

        switch (holder.getItemViewType()){
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((MessageReceive)holder).bind(discussion);
                break;
            case VIEW_TYPE_MESSAGE_SENT:

                ((MessageSend)holder).bind(discussion);

        }

    }
                @Override
    public int getItemCount() {
                    Log.w("count", "getMessageSize: "+discussionList.size() );
        return discussionList.size();
    }


    public class MessageSend  extends RecyclerView.ViewHolder{
        private TextView myMessage;

        public MessageSend(@NonNull View itemView) {
            super(itemView);
            myMessage=itemView.findViewById(R.id.chat_client_send);
        }

        void bind(Discussion discussion){
            myMessage.setText(discussion.getContent());

        }

    }

    public class MessageReceive  extends RecyclerView.ViewHolder{
        private TextView myMessage;
        public MessageReceive(@NonNull View itemView) {
            super(itemView);
            myMessage=itemView.findViewById(R.id.chat_client_receive);
        }

        void bind(Discussion discussion){
            Log.d("adapter", "bind: "+discussion.getContent());
            myMessage.setText(discussion.getContent());
        }

    }
}
