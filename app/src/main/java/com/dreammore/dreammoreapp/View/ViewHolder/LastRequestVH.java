package com.dreammore.dreammoreapp.View.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dreammore.dreammoreapp.R;

public class LastRequestVH extends RecyclerView.ViewHolder {

    public TextView request_subject,readState;
    private ImageView status_icon;
    public LastRequestVH(@NonNull View itemView) {
        super(itemView);
        request_subject=itemView.findViewById(R.id.l_request_row_subject);
        status_icon=itemView.findViewById(R.id.l_ic_requestStatus);


    }

    public TextView getRequest_subject() {
        return request_subject;
    }

    public void setRequest_subject(TextView request_subject) {
        this.request_subject = request_subject;
    }

    public ImageView getStatus_icon() {
        return status_icon;
    }

    public void setStatus_icon(ImageView status_icon) {
        this.status_icon = status_icon;
    }

    public TextView getReadState() {
        return readState;
    }

    public void setReadState(TextView readState) {
        this.readState = readState;
    }
}
