package com.dreammore.dreammoreapp.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.dreammore.dreammoreapp.Helpers.Utils;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.SingleActivity;

import es.dmoral.toasty.Toasty;

public class MobileTransactionActivity extends AppCompatActivity {

    private WebView payWebView;
    private Toolbar toolbar;
    private int subId;
    private String paymentUrl;
    private String identifier;
    private int draft_id;
    private ProgressBar _linkIsLoaded;
    private boolean linkLoaded;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_transaction);
        initActivityComponents();
        Intent intent = getIntent();
        this.paymentUrl= intent.getStringExtra("url");
        this.identifier= intent.getStringExtra("identifier");
        this.draft_id = intent.getIntExtra("draft_id",0);
        this.subId=intent.getIntExtra("subId",0);
        Log.d("paymentUrl", "onCreate: "+this.paymentUrl);
        Log.d("identifier", "onCreate: "+this.identifier);
        Log.d("subId", "onCreate: "+this.subId);
        if (Utils.checkInternet()){
            initWebView(this.paymentUrl);
        }else{
            //TODO no connection url
        }



    }

    private void initActivityComponents(){
        toolbar=findViewById(R.id.PayToolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.putExtra("identifier",identifier);
                intent.putExtra("subId",subId);
                setResult(RESULT_CANCELED,intent);
                finish();
                onBackPressed();

            }
        });
        payWebView=findViewById(R.id.payWebView);
        this._linkIsLoaded=findViewById(R.id.isLoaded);
        this.linkLoaded=false;
    }

    private void initWebView(String paymentUrl){
        payWebView.getSettings().setJavaScriptEnabled(true);
        payWebView.loadUrl(paymentUrl);
        payWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                _linkIsLoaded.setVisibility(View.GONE);
                payWebView.setVisibility(View.VISIBLE);

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent();
        intent.putExtra("identifier",identifier);
        intent.putExtra("subId",subId);
        setResult(RESULT_CANCELED,intent);
        this.finish();
        super.onBackPressed();

    }


}


