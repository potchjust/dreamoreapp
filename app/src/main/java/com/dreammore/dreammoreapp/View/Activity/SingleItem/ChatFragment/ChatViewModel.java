package com.dreammore.dreammoreapp.View.Activity.SingleItem.ChatFragment;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.dreammore.dreammoreapp.Model.Retrofit.Result;
import com.dreammore.dreammoreapp.Repository.*;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.ui.single.Fragment.SingleRepository;

import java.util.List;

public class ChatViewModel extends ViewModel {

    private RequestRepository requestRepository;
    private SingleRepository singleRepository;
    public MutableLiveData<Integer> admin_id=new MutableLiveData<Integer>();

    public ChatViewModel(){
        this.requestRepository=RequestRepository.getInstance();
        this.singleRepository=SingleRepository.getInstance();
    }
    public MutableLiveData<List<Discussion>> getDiscussions( int client_id,int request_id){
        return this.requestRepository.getRequestDiscussion(client_id,request_id);
    }

    public MutableLiveData<Result> sendResponseto(int customer_id, int request_id, int admin_id, String content){
        return this.singleRepository.respondeTo(customer_id, request_id, admin_id, content);
    }

    public void setTrueAdminId(int id){
        admin_id.setValue(id);
    }

    public MutableLiveData<Integer> getAdmin_id() {
        return admin_id;
    }
}
