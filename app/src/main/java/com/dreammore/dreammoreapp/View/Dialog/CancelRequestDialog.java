package com.dreammore.dreammoreapp.View.Dialog;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Model.Requete;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancelRequestDialog extends DialogFragment {

    private Button send;
    private TextInputEditText suggestions;
    private TextInputLayout suggestionLayout;
    private String commentaire;
    private AppClient appClient;
    private int cancel_id;
    private TextWatcher editextWatcher;

    public CancelRequestDialog(int id) {
        this.cancel_id=id;
    }

    private void initComponent(View view){
        this.send=view.findViewById(R.id.btn_dlg_ok);
        this.suggestions=view.findViewById(R.id.dlg_cancel_field);
        this.appClient= AppService.initService(AppClient.class);
        this.suggestionLayout=view.findViewById(R.id.dlg_cancel_fieldLayout);
        this.suggestionLayout.setErrorEnabled(true);

        this.suggestions.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("Editext", "before");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("Editext", "onTextChanged: ");
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable !=null){
                    if (editable.toString().length()>=150){
                       suggestions.setEnabled(false);
                    }

                }
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.cancel_request_dialog,container,false);
        initComponent(view);
        return  view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        this.send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refuseRequest(view);
            }
        });
    }


    public static CancelRequestDialog CancelRequest(FragmentManager fragmentManager,int id){
        CancelRequestDialog cancelRequestDialog=new CancelRequestDialog(id);
        cancelRequestDialog.show(fragmentManager,"cancel_dialog");
        return  cancelRequestDialog;
    }


    private void refuseRequest(View view){
        Log.d("request_id", "refuseRequest: "+this.cancel_id);

        this.appClient.CancelMyBill(this.cancel_id,this.suggestions.getText().toString()).enqueue(new Callback<Requete>() {
            @Override
            public void onResponse(Call<Requete> call, Response<Requete> response) {
                if (response.isSuccessful()){
                    startActivity(new Intent(getContext(),HomeActivity.class));
                    dismiss();
                }else{
                    Log.d("Cancel", "onResponse: CancelNo "+response.message());
                }
            }

            @Override
            public void onFailure(Call<Requete> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


}
