package com.dreammore.dreammoreapp.View.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Adapter.LastRequestAdapter;
import com.dreammore.dreammoreapp.View.Adapter.SliderAdapter;
import com.dreammore.dreammoreapp.View.Dialog.AddRequestDialog;
import com.dreammore.dreammoreapp.ViewModel.FragmentRequestViewModel;

import java.util.List;


/*code epuré*/
public class HomeFragment extends Fragment {

    private RecyclerView lrecycler;
    private LinearLayoutCompat container;
    private LinearLayout empty;
    private ProgressBar progressBar;
    private LinearLayoutCompat progressContainer;
    private LinearLayoutManager linearLayoutManager;
    private AppClient appClient;
    private FragmentRequestViewModel fragmentRequestViewModel;
    private Session session;
    private ViewPager2 viewPager;

    private Button btn_addRequest;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        initComponents(view);
        initVariable();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.loadLastRequest(session.readInt("client_id"));
        this.btn_addRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddRequestDialog.display(getFragmentManager());
            }
        });
    }

    private void initComponents(View view){
        this.lrecycler=view.findViewById(R.id.lrequestListView);
        this.linearLayoutManager=new LinearLayoutManager(getContext());
        this.btn_addRequest=view.findViewById(R.id.addrequest);
        this.lrecycler.setLayoutManager(linearLayoutManager);
        this.viewPager=view.findViewById(R.id.homeScreenSlider);
        SliderAdapter sliderAdapter=new SliderAdapter(getContext(),viewPager);
        this.container=view.findViewById(R.id.Listcontainer);
        this.container.setVisibility(View.INVISIBLE);
        this.btn_addRequest.setVisibility(View.GONE);
        this.viewPager.setAdapter(sliderAdapter);
        this.progressContainer=view.findViewById(R.id.progressContainer);
        this.progressBar=view.findViewById(R.id.homeProgress);
        this.empty=view.findViewById(R.id.emptyState);
    }

    private void initVariable(){
       fragmentRequestViewModel= ViewModelProviders.of(this).get(FragmentRequestViewModel.class);
       session=new Session(getContext());
    }

    private void loadLastRequest(int client_id){
        if (progressBar.isShown()){
            this.fragmentRequestViewModel.getMyRequests(client_id).observe(getViewLifecycleOwner(), new Observer<List<Draft>>() {
                @Override
                public void onChanged(List<Draft> draftList) {
                    progressContainer.setVisibility(View.GONE);
                        if (draftList.isEmpty()){
                            empty.setVisibility(View.VISIBLE);
                            container.setVisibility(View.VISIBLE);
                            btn_addRequest.setVisibility(View.VISIBLE);
                        }else{
                            container.setVisibility(View.VISIBLE);
                            btn_addRequest.setVisibility(View.VISIBLE);
                            empty.setVisibility(View.GONE);
                            LastRequestAdapter lastRequestAdapter=new LastRequestAdapter(getContext(),draftList);
                            lrecycler.setAdapter(lastRequestAdapter);
                            lrecycler.setLayoutManager(linearLayoutManager);
                        }

                }
            });
        }


    }

}
