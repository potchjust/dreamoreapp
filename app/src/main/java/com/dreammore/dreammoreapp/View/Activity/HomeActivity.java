/**
 * TODO faire une recherchz approfondi sur les onsavedInstanced
 */
package com.dreammore.dreammoreapp.View.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Helpers.Utils;
import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.Model.Retrofit.SlideResult;
import com.dreammore.dreammoreapp.R;

import com.dreammore.dreammoreapp.View.Adapter.LastRequestAdapter;
import com.dreammore.dreammoreapp.View.Adapter.SliderAdapter;
import com.dreammore.dreammoreapp.View.Dialog.AddRequestDialog;
import com.dreammore.dreammoreapp.ViewModel.FragmentRequestViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;*/

public class HomeActivity extends AppCompatActivity implements LifecycleEventObserver,SwipeRefreshLayout.OnRefreshListener {
    /**
     * code de retour
     */

    private boolean isRefreshed=false;
    private final static String CAPTURED_PHOTO_PATH_KEY="mCurrentPhotoPath";
    private final static String CAPTURED_PHOTO_URI_KEY="mCurrentPhotoUri";
    private static List<Uri> uploadedFileList = new ArrayList<>();
    private final int GALLERY_REQUEST_CODE = 200;
    private final int PICTURE_REQUEST_CODE = 201;
    private final int AUDIO_REQUEST_CODE = 202;
    private String link;
    private Intent intent;
    private Uri imageUri=null;
    private String imagePath=null;
    private Button addRequest;
    private List<Uri>slideUri=new ArrayList<>();
    private WorkManager workManager;
    private Context context;
    private Session session;
    private AppClient appClient;
    private AppService appService;
    private LastRequestAdapter lastRequestAdapter;
    private TextView notRead;
    private LinearLayoutManager linearLayoutManager;
    private SliderAdapter sliderAdapter;
    private PeriodicWorkRequest periodicWorkRequest;
    private List<SlideResult>slideResultList=new ArrayList<>();
    private RecyclerView requestList;
    private FragmentRequestViewModel requestViewModel;
    private int client_id;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TabLayout homeTabLayout;
    private boolean isLoaded=false;
    private ViewPager2 slider;
    private LinearLayout emptyStateLayout;
    private MaterialButton btn_writeRequest;
    private LinearLayout progressContainer;
    private LinearLayoutCompat listContainer;
    private ProgressBar homeProgress;
    private RecyclerView recyclerView;
    private Runnable sliderunnable;
    private SwipeRefreshLayout container;

    public static List<Uri> getUploadedFileList() {
        return uploadedFileList;
    }

    public static int UploadFileCount() {
        return uploadedFileList.size();
    }

    private static Context getContextOfApplication() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.session = new Session(this);
        context = getApplicationContext();
        Utils.checkUpdate(this);

        initComponents();
        initVariables();
        Log.d("isRefresh", "BeforeChanged: "+isRefreshed);
        loadSlide();
        getToken();
        this.appClient=AppService.initService(AppClient.class);

        intent=getIntent();





    }

    /**
     * @param requestCode qui doit correspondre au code qui aura été passé de l'autre coté
     * @param resultCode  pour savoir si l'operation s'est bien passé
     * @param data        qui sont les donnéé retourné
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /**
         * si l'opération d'envoi s'est bien passé alors
         * on verifie le code de retour afin de savoir quelle opération faire
         */
        if (resultCode == -1) {
            switch (requestCode) {
                case AUDIO_REQUEST_CODE:
                    Uri uri=data.getData();
                    Log.d("AudioUri", "onActivityResult: "+uri);
                    uploadedFileList.add(uri);
                    break;
                case GALLERY_REQUEST_CODE:
                    Uri galleryUri = data.getData();
                    Log.d("Gallery", "onActivityResult: " + galleryUri);
                    uploadedFileList.add(galleryUri);
                    break;
                case PICTURE_REQUEST_CODE:
                    uploadedFileList.add(getImageUri());
                    Log.d("PhotoUriget", "onActivityResult: "+getImageUri());
                    break;
            }
        }

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Dreammore");
        builder.setMessage("Voulez vous quitter l'application ?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();


    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle saveInstanceState) {
        if (imagePath!=null){
            saveInstanceState.putString(CAPTURED_PHOTO_PATH_KEY,imagePath);
        }
        if (imageUri!=null){
            saveInstanceState.putString(CAPTURED_PHOTO_URI_KEY,imageUri.toString());
        }
        super.onSaveInstanceState(saveInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        if (savedInstanceState.containsKey(CAPTURED_PHOTO_PATH_KEY)){
            imagePath=savedInstanceState.getString(CAPTURED_PHOTO_PATH_KEY);
        }
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_URI_KEY)){
            imageUri=Uri.parse(savedInstanceState.getString(CAPTURED_PHOTO_URI_KEY));
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }


    private void getToken(){
        FirebaseMessaging.getInstance().subscribeToTopic("notification").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                }else{

                }
            }
        });
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token=instanceIdResult.getToken();
                Log.d("Token", "onSuccess: "+token);
                session.save("fcmToken",token);
                appClient.SaveFcmToken(session.readInt("client_id"),token).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()){
                            Log.d("getToken", "onResponse: Peace and love");
                        }else{
                            Log.d("getToken", "onResponse: Peace and love"+response.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

            }
        });
    }


    private void initComponents(){
        this.swipeRefreshLayout=findViewById(R.id.homeContainer);
        this.swipeRefreshLayout.setOnRefreshListener(this);
        this.addRequest = findViewById(R.id.addrequest);
        this.linearLayoutManager=new LinearLayoutManager(this);
        // this.swipeRefreshLayout=findViewById(R.id.homeSwipeRefresh);
        this.requestList=findViewById(R.id.requestListView);
        this.homeProgress=findViewById(R.id._homeProgress);
        this.slider=findViewById(R.id._homeScreenSlider);
        this.emptyStateLayout=findViewById(R.id.emptyState);
        this.progressContainer=findViewById(R.id._progressContainer);
        this.btn_writeRequest=findViewById(R.id.btn_addrequest);
        this.listContainer=findViewById(R.id._listcontainer);
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                Log.d("notif_data", "Key: " + key + " Value: " + value);
            }
        }else{
            Log.d("notif_data", "data");
        }
      /*  swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (swipeRefreshLayout.isRefreshing()){
                    Toast.makeText(context,"isREfress",Toast.LENGTH_SHORT).show();
                    loadSlide();
                    if (isRefreshed){
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            }
        });*/
    }

    private void initVariables(){
        this.client_id=session.readInt("client_id");
        this.requestViewModel= ViewModelProviders.of(this).get(FragmentRequestViewModel.class);
        if (Utils.checkInternet()){
            this.requestViewModel.getLastRequest(client_id).observe(this, new Observer<List<Draft>>() {
                @Override
                public void onChanged(List<Draft> draftList) {
                    if (draftList == null) {
                        progressContainer.setVisibility(View.GONE);
                        emptyStateLayout.setVisibility(View.VISIBLE);
                        btn_writeRequest.setVisibility(View.VISIBLE);
                    } else {
                        progressContainer.setVisibility(View.GONE);
                        listContainer.setVisibility(View.VISIBLE);
                        emptyStateLayout.setVisibility(View.GONE);
                        btn_writeRequest.setVisibility(View.VISIBLE);
                        lastRequestAdapter = new LastRequestAdapter(getApplicationContext(), draftList);
                        requestList.setAdapter(lastRequestAdapter);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                        requestList.setLayoutManager(linearLayoutManager);
                    }

                }
            });
        }else{
            new AlertDialog.Builder(HomeActivity.this)
                    .setTitle("Dreammore")
                    .setMessage("Problème de connection")
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(HomeActivity.this, HomeActivity.class));
                        }
                    }).show();
        }

        /*show the request dialog view*/
        this.btn_writeRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddRequestDialog.display(getSupportFragmentManager());
                //PaymentDialog.displayDialog(getSupportFragmentManager());
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(sliderunnable,3500);
    }



    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new Handler().removeCallbacks(sliderunnable);
    }

    @Override
    public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {

    }


    private void loadSlide(){
        this.requestViewModel.getSlide().observe(this, new Observer<List<SlideResult>>() {
            @Override
            public void onChanged(List<SlideResult> slideResults) {
                if (!slideResults.isEmpty()){
                    Log.d("isRefresh", "onChanged: "+isRefreshed);
                    sliderAdapter=new SliderAdapter(context,slider,slideResults);
                    isRefreshed=true;
                    slider.setAdapter(sliderAdapter);
                    slider.setClipToPadding(false);
                    slider.setClipChildren(false);
                    slider.setOffscreenPageLimit(3);
                    slider.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

                    CompositePageTransformer compositePageTransformer=new CompositePageTransformer();
                    compositePageTransformer.addTransformer(new MarginPageTransformer(40));
                    compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
                        @Override
                        public void transformPage(@NonNull View page, float position) {
                            float r= 1 - Math.abs(position);
                            page.setScaleY(0.85f + r * 0.15f);
                        }
                    });
                    slider.setPageTransformer(compositePageTransformer);
                    slider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                        @Override
                        public void onPageSelected(int position) {
                            super.onPageSelected(position);
                            new Handler().removeCallbacks(sliderunnable);
                            new Handler().postDelayed(sliderunnable,3500);
                        }
                    });

                    sliderunnable=new Runnable() {
                        @Override
                        public void run() {
                            slider.setCurrentItem(slider.getCurrentItem()+1);
                        }
                    };

                }else{

                }
            }
        });

    }


    @Override
    public void onRefresh() {
        Log.d("isCalled","ds");
        if (swipeRefreshLayout.isRefreshing()){
            loadSlide();
            if (isRefreshed){
                swipeRefreshLayout.setRefreshing(false);
                Toasty.success(this,"Mise à jour effectuer",Toasty.LENGTH_LONG).show();
            }
        }
    }
}
