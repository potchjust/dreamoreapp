package com.dreammore.dreammoreapp.View;


import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.dreammore.dreammoreapp.BuildConfig;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Helpers.Utils;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.dreammore.dreammoreapp.View.Activity.OtpSendActivity;
import com.dreammore.dreammoreapp.View.Activity.RegisterActivity;
import com.dreammore.dreammoreapp.View.Activity.WelcomeActivity;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;

import es.dmoral.toasty.Toasty;

public class SplashScreenActivity extends AppCompatActivity {
    private Animation animation;
    private ObjectAnimator objectAnimator;
    private ImageView logoView;
    private Handler handler;

    private Session session;
    private static int update_int=001;

    private AppUpdateManager appUpdateManager;


    @Override
    protected void onStart() {
        super.onStart();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (Utils.checkInternet()){
                    if (checkConnected()) {
                        startActivity(new Intent(SplashScreenActivity.this, HomeActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(SplashScreenActivity.this, OtpSendActivity.class));
                        finish();
                    }
                }else{
                    //TODO create an network activity
                    new AlertDialog.Builder(SplashScreenActivity.this)
                            .setTitle("Information")
                            .setMessage("Veuillez vérifier votre connexion internet")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    startActivity(new Intent(SplashScreenActivity.this,SplashScreenActivity.class));
                                }
                            }).show();

                }

              /*  if (checkConnected()) {
                    startActivity(new Intent(SplashScreenActivity.this, HomeActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashScreenActivity.this, RegisterActivity.class));
                    finish();
                }*/
            }
        };
        /**
         * Affiche la classe correspondante après 2 secondes
         */
        Log.d("SplashScreen", "onStart: ");
        new Handler().postDelayed(runnable, 2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        this.objectAnimator = new ObjectAnimator();
        session = new Session(this);
        Utils.checkUpdate(this);
        Log.d("checkConnected", "onStart: "+this.checkConnected());
        logoView = findViewById(R.id.dm_logo_splash);
        Fresco.initialize(this);



    }

    private boolean checkConnected() {
        if (session.readInt("client_id") != 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Dreammore:Information");
        builder.setCancelable(false);
        builder.setMessage("Voulez vous quitter l'application ?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("SplashQuit", "onBackPressed: Application quit");
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkServicePlay();



    }

    public boolean checkServicePlay(){
        GoogleApiAvailability googleApiAvailability=GoogleApiAvailability.getInstance();
        int status=googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status!= ConnectionResult.SUCCESS){
            if (googleApiAvailability.isUserResolvableError(status)){
                googleApiAvailability.getErrorDialog(this,status,2404).show();
            }
            Log.d("play", "checkServicePlay: Mo");
            return false;
        }else{
            Log.d("play", "checkServicePlay: True");
            return true;
        }
    }





}
