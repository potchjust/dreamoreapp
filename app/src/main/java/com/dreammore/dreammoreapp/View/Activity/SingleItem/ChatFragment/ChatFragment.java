//TODO revoir la partie chat
/*
* la partie chat devra fonctionner de la maniere qui suit
* la reference doit etre en fonction de la requete
*
* */
package com.dreammore.dreammoreapp.View.Activity.SingleItem.ChatFragment;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dreammore.dreammoreapp.Helpers.FirebaseHelpers;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Helpers.UnicodeEmojiCategoryPagerFragment;
import com.dreammore.dreammoreapp.Model.Firebase.Messages;
import com.dreammore.dreammoreapp.Model.Retrofit.Result;
import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.SingleActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.lytefast.flexinput.InputListener;
import com.lytefast.flexinput.adapters.AddContentPagerAdapter;
import com.lytefast.flexinput.adapters.AttachmentPreviewAdapter;
import com.lytefast.flexinput.adapters.EmptyListAdapter;
import com.lytefast.flexinput.fragment.CameraFragment;
import com.lytefast.flexinput.fragment.FilesFragment;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.fragment.PhotosFragment;
import com.lytefast.flexinput.managers.KeyboardManager;
import com.lytefast.flexinput.managers.SimpleFileManager;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.model.Attachment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class ChatFragment extends Fragment {

    private List<Discussion>_discussion;
    private LinearLayoutManager linearLayoutManager;
    private ChatViewModel mViewModel;
    private FirebaseHelpers firebaseHelpers;
    private Session session;
    private int client_id;
    private int draft_id;
    private TextView emptyText;
    private RecyclerView discussionList;
    private ChatAdapter chatAdapter;
    private List<Messages> discussionListTest;
    private RelativeLayout chatBoxContainer;
    private ProgressDialog progressDialog;
    private Button btn_sendResponse;
    private TextInputEditText edt_chatBox;
    private SingleActivity activity;
    private LinearLayout chatState;
    private NestedScrollView scrollView;
    private int messagesSize;
    public int adminId;
    private LinearLayout respondeZone;
    private FlexInputFragment flexInputFragment;

    private static ChatFragment chatFragmentIntance;

    public ChatFragment(){

    }

    public static ChatFragment getInstance(){
        if (chatFragmentIntance==null){
            chatFragmentIntance=new ChatFragment();
        }
        return chatFragmentIntance;
    }

    public ChatFragment(SingleActivity singleActivity){
        this.activity=singleActivity;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.single_chat_fragment, container, false);
        initFragmentComponent(view);

        initFirebase();
        Log.d("draft_id", "onActivityCreated: "+activity.getDraft_id());
        return view;
    }

    private void initFragmentComponent(View view) {
        this.session = new Session(getContext());
        this.client_id = session.readInt("client_id");
        this.draft_id = session.readInt("draft_id");
        this.discussionList = view.findViewById(R.id.reyclerview_message_list);
        this.discussionListTest = new ArrayList<>();
        this.progressDialog = new ProgressDialog(getContext());
        this.linearLayoutManager = new LinearLayoutManager(getContext());
        this.discussionList.setLayoutManager(linearLayoutManager);
       /* this.btn_sendResponse = view.findViewById(R.id.button_chatbox_send);*/
        mViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
      /*  this.edt_chatBox = view.findViewById(R.id.edittext_chatbox);*/
        _discussion=new ArrayList<>();
        this.chatAdapter=new ChatAdapter(_discussion,getContext());
        discussionList.setAdapter(chatAdapter);
        this.linearLayoutManager.setReverseLayout(false);
        this.linearLayoutManager.setStackFromEnd(true);
        scrollView=view.findViewById(R.id.scroll);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.progressDialog.setTitle("Dreammore");
        this.progressDialog.setMessage("Merci de patienter durant l'affichage de votre discussion");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final InputMethodManager imm =
                (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        flexInputFragment = (FlexInputFragment) getChildFragmentManager().findFragmentById(R.id.flex_input);
        if (savedInstanceState == null) {
            // Only create fragment on first load
            // UnicodeEmojiCategoryPagerFragment is a default implementation (see sample app)
            flexInputFragment.setEmojiFragment(new UnicodeEmojiCategoryPagerFragment());
        }

        flexInputFragment
                .setContentPages(/* You can add custom PageSuppliers here */)
                .setInputListener(flexInputListener)
                .setFileManager(new SimpleFileManager("com.lytefast.flexinput.fileprovider", "FlexInput"))
                .setKeyboardManager(new KeyboardManager() {
                    @Override
                    public void requestDisplay(@NotNull EditText editText) {
                        if (editText == null){
                            return;
                        }
                        imm.showSoftInput(flexInputFragment.getView(), InputMethodManager.SHOW_IMPLICIT);
                    }


                    @Override
                    public void requestHide() {
                        imm.hideSoftInputFromWindow(flexInputFragment.getView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                });

        optionalFeatures();
        tryRiskyFeatures();

    }


    private final InputListener flexInputListener = new InputListener() {
        @Override
        public boolean onSend(final Editable data, List<? extends Attachment<?>> attachments) {

            Log.d("SAMPLE", "User sent: " + data.toString());

            for (int i = 0; i < attachments.size(); i++) {
                Log.d("SAMPLE", String.format(" Attachment - %s"));
            }
            return false;
        }
    };

    private void optionalFeatures() {
        flexInputFragment
                // Can be extended to provide custom previews (e.g. larger preview images, onclick) etc.
                .setAttachmentPreviewAdapter(new AttachmentPreviewAdapter(getContext().getContentResolver()));
        final boolean hasCustomPages = false;
        if (hasCustomPages) {
            flexInputFragment.setContentPages(createContentPages());
        }
    }


    private void loadMessages(){

        this.mViewModel.getDiscussions(session.readInt("client_id"),activity.getDraft_id()).observe(getViewLifecycleOwner(), new Observer<List<Discussion>>() {
            @Override
            public void onChanged(List<Discussion> discussions) {

                if (discussions.isEmpty()){
                    chatState.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }else{
                    chatState.setVisibility(View.GONE);
                    chatAdapter=new ChatAdapter(discussions,getContext());
                    discussionList.setAdapter(chatAdapter);
                }

            }
        });
    }

    /**
     * Not necessary if the defaults are sufficient. Add to this array if custom pages needed.
     */
    private static AddContentPagerAdapter.PageSupplier[] createContentPages() {
        return new AddContentPagerAdapter.PageSupplier[]{
                new AddContentPagerAdapter.PageSupplier(R.drawable.ic_image_24dp, R.string.attachment_photos) {
                    @Override
                    @NotNull
                    public Fragment createFragment() {
                        return new PhotosFragment();
                    }
                },
                new AddContentPagerAdapter.PageSupplier(R.drawable.ic_file_24dp, R.string.attachment_files) {
                    @Override
                    @NotNull
                    public Fragment createFragment() {
                        return new CustomFilesFragment();
                    }
                },
                new AddContentPagerAdapter.PageSupplier(R.drawable.ic_add_a_photo_24dp, R.string.attachment_camera) {
                    @Override
                    @NotNull
                    public Fragment createFragment() {
                        return new CameraFragment();
                    }
                }
        };
    }

    public static class CustomFilesFragment extends FilesFragment {
        @Override
        @NotNull
        protected EmptyListAdapter newPermissionsRequestAdapter(final View.OnClickListener onClickListener) {
            return new EmptyListAdapter(
                    R.layout.custom_permission_storage, R.id.permissions_req_btn, onClickListener);
        }
    }

    private void tryRiskyFeatures() {
        final boolean hasCustomEditText = false;
        if (hasCustomEditText) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            AppCompatEditText myEditText = (AppCompatEditText) inflater.inflate(
                    R.layout.my_edit_text_view, (ViewGroup) flexInputFragment.getView(), false);
            flexInputFragment.setEditTextComponent(myEditText);
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        flexInputFragment.requestFocus();
    }




    private void initFirebase(){
        firebaseHelpers=FirebaseHelpers.getInstance();
    }

}
