package com.dreammore.dreammoreapp.View.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.bumptech.glide.annotation.GlideModule;
import com.dreammore.dreammoreapp.Helpers.AppGlide;
import com.dreammore.dreammoreapp.Model.Retrofit.SlideResult;
import com.dreammore.dreammoreapp.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.SliderViewHolder> {

    private ViewPager2 viewPager2;
    private Context context;
    private List<SlideResult>slideUrl;


    public SliderAdapter(Context context, ViewPager2 viewPager, List<SlideResult> slideUrl) {
        this.context = context;
        this.viewPager2=viewPager;
        this.slideUrl=slideUrl;

    }


    public SliderAdapter(Context context, ViewPager2 viewPager) {
        this.context = context;
        this.viewPager2 = viewPager;
    }

    @NonNull
    @Override
    public SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.slidelayout,parent,false);
        return  new SliderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SliderViewHolder holder, int position) {

        SlideResult slideResult=slideUrl.get(position);

        Glide
                .with(this.context)
                .load(slideResult.getUrl())
                .fitCenter()
                .into(holder.sample);


        if (position == slideUrl.size() - 1){
            viewPager2.post(sliderRunnable);
        }

    }

    @Override
    public int getItemCount() {
        return slideUrl.size();
    }


     class SliderViewHolder extends RecyclerView.ViewHolder{

        private ImageView sample;

        public SliderViewHolder(@NonNull View itemView) {
            super(itemView);
            this.sample=itemView.findViewById(R.id.SlideImageView);
        }

    }


    private Runnable sliderRunnable=new Runnable() {
        @Override
        public void run() {
            slideUrl.addAll(slideUrl);
            notifyDataSetChanged();
        }
    };
}
