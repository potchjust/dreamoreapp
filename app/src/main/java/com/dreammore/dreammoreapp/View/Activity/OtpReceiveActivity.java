package com.dreammore.dreammoreapp.View.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.dreammore.dreammoreapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseAuthProvider;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

public class OtpReceiveActivity extends AppCompatActivity {

    private TextInputEditText code;
    private FirebaseAuth firebaseAuth=FirebaseAuth.getInstance();
    private FirebaseUser firebaseUser;
    private MaterialButton checkButton;
    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_receive);
        code=findViewById(R.id.receiveCode);
        checkButton=findViewById(R.id.checkCode);
        progressDialog = new ProgressDialog(this);

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConstraintLayout constraintLayout = findViewById(R.id.otpReceiveLayoutId);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(constraintLayout.getWindowToken(), 0);
                progressDialog.setTitle("Dreammore");
                progressDialog.setMessage("Vérification de votre code");
                progressDialog.show();
                String verification_code = getIntent().getStringExtra("verificationId");
                checkIfisVerified(verification_code,code.getText().toString(),getIntent().getStringExtra("phoneNumber"));
            }
        });


    }


    private void checkIfisVerified(String verificationId,String code,String phoneNumber) {
        Log.d("phoneNumber",code);

        try {
            PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(verificationId,code);
            firebaseAuth.signInWithCredential(phoneAuthCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    progressDialog.dismiss();
                    if (task.isSuccessful()){
                        if (task.getResult().getUser() != null){
                            Intent intent = new Intent(OtpReceiveActivity.this,RegisterActivity.class);
                            intent.putExtra("phoneNumber",phoneNumber);
                            startActivity(intent);
                        }
                    }else{
                        Snackbar errorCode =Snackbar.make(findViewById(android.R.id.content).getRootView(),"Code saisi est erroné", Snackbar.LENGTH_LONG);
                        View snackbarView= errorCode.getView();
                        snackbarView.setBackgroundColor(ContextCompat.getColor(OtpReceiveActivity.this, R.color.colorDanger));
                        errorCode.show();

                    }

                }
            });

        }catch (Exception ignored){
            Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),"Code erroné",Snackbar.LENGTH_LONG).show();
        }




    }
}
