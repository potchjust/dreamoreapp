package com.dreammore.dreammoreapp.View.Dialog;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.dreammore.dreammoreapp.R;

import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.SingleActivity;

public class ValidateRequestDialog extends DialogFragment {

    private Button btn_ok;


    private int validate_id;
    private int price;
    public ValidateRequestDialog(int id,int price){
        this.validate_id=id;
        this.price=price;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(),HomeActivity.class));
                 closeDialog(view);
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.popup_request_v,container,false);
        initDialogComponent(view);
        return  view;
    }


    private void initDialogComponent(View view){
            this.btn_ok=view.findViewById(R.id.btn_ok);
    }


    public ValidateRequestDialog display(FragmentManager fragmentManager){
        ValidateRequestDialog validateRequestDialog=new ValidateRequestDialog(validate_id,price);
        validateRequestDialog.show(fragmentManager,"ValidateRequestDialog");
        return validateRequestDialog;
    }

    public void closeDialog(View view){
       dismiss();
    }
}
