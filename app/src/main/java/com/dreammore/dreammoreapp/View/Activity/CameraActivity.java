package com.dreammore.dreammoreapp.View.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;

import com.dreammore.dreammoreapp.R;

import java.net.URI;

public class CameraActivity extends AppCompatActivity {

    private final static String CAPTURED_PHOTO_PATH_KEY="mCurrentPhotoPath";
    private final static String CAPTURED_PHOTO_URI_KEY="mCurrentPhotoUri";

    private String mCurrentPhotoPath=null;
    private Uri mCapturedImageUri=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle saveInstanceState) {
        if (mCurrentPhotoPath!=null){
            saveInstanceState.putString(CAPTURED_PHOTO_PATH_KEY,mCurrentPhotoPath);
        }
        if (mCapturedImageUri!=null){
            saveInstanceState.putString(CAPTURED_PHOTO_URI_KEY,mCapturedImageUri.toString());
        }
        super.onSaveInstanceState(saveInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        if (savedInstanceState.containsKey(CAPTURED_PHOTO_PATH_KEY)){
            mCurrentPhotoPath=savedInstanceState.getString(CAPTURED_PHOTO_PATH_KEY);
        }
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_URI_KEY)){
            mCapturedImageUri=Uri.parse(savedInstanceState.getString(CAPTURED_PHOTO_URI_KEY));
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    public String getmCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public void setmCurrentPhotoPath(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    public Uri getmCapturedImageUri() {
        return mCapturedImageUri;
    }

    public void setmCapturedImageUri(Uri mCapturedImageUri) {
        this.mCapturedImageUri = mCapturedImageUri;
    }
}
