package com.dreammore.dreammoreapp.View.Activity.SingleItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.dreammore.dreammoreapp.API.Client.AppClient;

import com.dreammore.dreammoreapp.API.Client.PayGateClient;
import com.dreammore.dreammoreapp.API.Service.AppService;

import com.dreammore.dreammoreapp.Helpers.Listener.OnSingleActivityFragmentListener;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Helpers.Utils;
import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.Model.Request;
import com.dreammore.dreammoreapp.Model.Retrofit.PaymentStatus;
import com.dreammore.dreammoreapp.R;
import com.dreammore.dreammoreapp.Repository.RequestRepository;

import com.dreammore.dreammoreapp.View.Activity.SingleItem.ChatFragment.ChatFragment;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.ui.single.Fragment.DetailsFragment.DetailsFragment;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.ui.single.Fragment.DetailsFragment.DetailsViewModel;
import com.google.android.material.tabs.TabLayout;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import es.dmoral.toasty.Toasty;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleActivity extends AppCompatActivity {

    private DetailsViewModel detailsViewModel;
    public static final int PAYPAL_REQUEST_CODE=7171;
    private static final int CLOSE_REQUEST_CODE=500;
    private SinglePageAdapter singlePageAdapter;
    private ViewPager singleActivityPager;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private Intent intent;
    private Session session;
    public int trueAdminId;
    private int draft_id;
    private int statut;
    private int montant;
    private Boolean hasBill;
    private int admin_id;
    private String bill;
    private AppClient appClient;
    private int subId;


    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public int getAdmin_id() {
        return admin_id;
    }

    public int getTrueAdmin_id(){
        return trueAdminId;
    }



    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public int getDraft_id() {
        return draft_id;
    }

    public int getMontant() {
        return montant;
    }

    public void setDraft_id(int draft_id) {
        this.draft_id = draft_id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_activity);
        initActivityComponent();
    }

    public Boolean getHasBill() {
        return hasBill;
    }

    public void setHasBill(Boolean hasBill) {
        this.hasBill = hasBill;
    }

    private void initActivityComponent() {
        singleActivityPager = findViewById(R.id.singleActivityViewPager);
        tabLayout = findViewById(R.id.singleTableLayout);
        toolbar = findViewById(R.id.singleActivityToolbar);
        setSupportActionBar(toolbar);
        intent=getIntent();
        session = new Session(this);
        appClient=AppService.initService(AppClient.class);

        FragmentManager fragmentManager=getSupportFragmentManager();
        //recuperation de la variable de l'intent
        //creation des fragments

        if (intent.getBooleanExtra("from_notif", false)){
            this.draft_id=intent.getIntExtra("notif_id",0);
            this.statut=intent.getIntExtra("notif_draft_state",0);
            this.montant=intent.getIntExtra("notif_draft_montant",0);
            this.admin_id=intent.getIntExtra("notif_admin_id",0);
            this.bill=intent.getStringExtra("attachment_url");

        }else{
            this.draft_id=intent.getIntExtra("draft_id",0);
            this.statut=intent.getIntExtra("draft_state",0);
            this.admin_id=intent.getIntExtra("admin_id",0);



        }

        singlePageAdapter = new SinglePageAdapter(getSupportFragmentManager(), this);
        singlePageAdapter.initFragment(new DetailsFragment(this), "Informations");
        singlePageAdapter.initFragment(new ChatFragment(this), "Discussion");
        singleActivityPager.setAdapter(singlePageAdapter);
        tabLayout.setupWithViewPager(singleActivityPager);
        /*get the draft_id that will be share by all component that is hosted by the activity*/
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }


    /*result*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("requestCode", "onActivityResult: "+requestCode);

        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                PaymentConfirmation confirmation=data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                /*show dialog*/
                if (confirmation!=null){
                    Toast.makeText(this,"Pay",Toast.LENGTH_LONG).show();
                }
            }else if (resultCode == Activity.RESULT_CANCELED){
                Toast.makeText(this,"CancelByUser",Toast.LENGTH_LONG).show();

            }
        }else if (resultCode ==PaymentActivity.RESULT_EXTRAS_INVALID){
            Toast.makeText(this,"Invalid",Toast.LENGTH_LONG).show();
        }

        /*partie de paygate*/
        if (requestCode==CLOSE_REQUEST_CODE) {
           if (resultCode==RESULT_CANCELED){
               Toasty.info(this,"ok",Toasty.LENGTH_LONG).show();
               checkPaymentStatus(data.getStringExtra("identifier"));
               subId=data.getIntExtra("subId",0);

           }
        }
    }

    @SuppressLint("CheckResult")
    private void checkPaymentStatus(String identifier){
        CompositeDisposable compositeDisposable=new CompositeDisposable();
        PayGateClient payGateClient= AppService.initService2(PayGateClient.class);
        payGateClient.getMobileMoneyStatus(Utils.payGateClientUrl,identifier)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<PaymentStatus>() {
                    @Override
                    public void accept(PaymentStatus paymentStatus) throws Exception {
                        if (paymentStatus!=null){
                            if (paymentStatus.getError_code()==403){
                                Toasty.error(SingleActivity.this,"Pas de transaction trouvé",Toasty.LENGTH_LONG).show();
                            }else{
                                Log.d("paymentStatus",Integer.toString(paymentStatus.getStatus()));
                                Log.d("paymentMethod",paymentStatus.getPayment_method());
                                updatePaymentStatus(subId,paymentStatus.getStatus(),identifier,paymentStatus.getPayment_method());

                            }
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void updatePaymentStatus(int payment_id,int status,String identifier,String payment_method){
            appClient.updatePaymentStatus(payment_id,status,identifier,payment_method)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<Integer>() {
                        @Override
                        public void accept(Integer integer) throws Exception {

                            /*En fonction des states*/
                            Log.d("AcceptPayment","status is"+status);
                            Log.d("AcceptPayment", "accept: "+integer);
                        }
                    });
    }

}

