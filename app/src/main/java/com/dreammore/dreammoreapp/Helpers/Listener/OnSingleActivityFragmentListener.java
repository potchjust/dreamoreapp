package com.dreammore.dreammoreapp.Helpers.Listener;

public interface OnSingleActivityFragmentListener {

    void onAdminIdChange(int adminId);

    void onUserIdChange(int changeId);
}
