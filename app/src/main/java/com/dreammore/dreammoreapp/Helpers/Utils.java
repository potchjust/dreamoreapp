package com.dreammore.dreammoreapp.Helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.InetAddresses;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.dreammore.dreammoreapp.R;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.testing.FakeAppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import es.dmoral.toasty.Toasty;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class Utils {


    private static Activity activity;

    public Utils() {
        activity = new Activity();
    }

    private static int update_int=001;
    private static AppUpdateManager appUpdateManager;
    public static String payGateClientUrl="38e4549c-6653-4ada-b2da-b1496729ab0f";

    private static Session session;
    public static boolean checkInternet(){
        InetAddress inetAddress=null;
        try{
            Future<InetAddress>future= Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() throws Exception {
                    try{
                        return InetAddress.getByName("google.com");
                    }catch (UnknownHostException exception){
                        return null;
                    }

                }
            });
            inetAddress=future.get(10, TimeUnit.SECONDS);
            future.cancel(true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return inetAddress!=null &&!inetAddress.equals("");
    }

    public static void checkUpdate(Context context){
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(context);

        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                Toasty.info(context,"update",Toasty.LENGTH_SHORT).show();
            }else{
                Toasty.info(context,"No update",Toasty.LENGTH_SHORT).show();
            }
        });

    }






   /* public static  void checkUpdate(Context context){
        FakeAppUpdateManager fakeAppUpdateManager = new FakeAppUpdateManager(context);
        fakeAppUpdateManager.setUpdateAvailable(2);
        Task<AppUpdateInfo> appUpdateInfoTask = fakeAppUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                Toasty.info(context,"update",Toasty.LENGTH_SHORT).show();
            }else{
                Toasty.info(context,"No update",Toasty.LENGTH_SHORT).show();
            }
        });
            }
*/



    private static void showUpdateAvailableNotification(Context context){
        String NOTIFICATION_CHANNEL_ID="com.dreammore.dreammoreapp";
        Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=" +context.getPackageName()));
        PendingIntent pendingIntent=PendingIntent.getActivity(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel=new NotificationChannel(NOTIFICATION_CHANNEL_ID,"com.dreammore.dreammoreapp",NotificationManager.IMPORTANCE_MAX);
            notificationChannel.enableLights(true);
            notificationChannel.setDescription("Update available");
            notificationChannel.enableVibration(true);
            notificationChannel.setLightColor(R.color.colorPrimary);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder mBuilder=new NotificationCompat.Builder(context,NOTIFICATION_CHANNEL_ID);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle("Information")
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentText("Une nouvelle mise à jour de votre application est disponible.Rendez vous sur Playstore pour l'obtenir ")
                .setSmallIcon(R.drawable.ic_system_update_primary_24dp)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(pendingIntent);
        Notification notification=mBuilder.build();
        notificationManager.notify(new Random().nextInt(),notification);
    }


    public static String getPath(Uri uri) {
        String result;
        //  String projection[] = {MediaStore.Images.Media.DATA};

        Cursor cursor = activity.getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(column_index);
            cursor.close();
        }
        return result;
    }

    private void uploadFile(Uri uri){
        File file=new File(Utils.getPath(uri));
        RequestBody requestedFile= RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body=MultipartBody.Part.createFormData("attachment",file.getName(),requestedFile);
    }


}
