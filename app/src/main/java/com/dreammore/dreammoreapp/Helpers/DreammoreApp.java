package com.dreammore.dreammoreapp.Helpers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.dreammore.dreammoreapp.DAO.RequestDao;
import com.dreammore.dreammoreapp.Model.Request;

@Database(entities = {Request.class},version = 1)
public abstract class DreammoreApp extends RoomDatabase {

    public abstract RequestDao requestDao();
    private static DreammoreApp databaseInstance;

    public static DreammoreApp getInstance(Context context){
            if (databaseInstance==null){
                databaseInstance=Room.databaseBuilder(context,DreammoreApp.class,"dreammore").build();
            }
            return  databaseInstance;
    }

}
