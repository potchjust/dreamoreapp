package com.dreammore.dreammoreapp.Helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Session {


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public Session(Context context) {
        sharedPreferences =context.getSharedPreferences("userInfo", context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * Sauvegarde des infos
     *
     * @param key   definit la clé
     * @param value to save which can be String|Integer|Boolean
     */
    public void save(String key, Object value) {
        if (value instanceof String) {
            Log.d("saving", "save: ");
            editor.putString(key, value.toString());
        } else if (value instanceof Integer) {
            Log.d("saving", "save: ");
            editor.putInt(key, Integer.parseInt(value.toString()));
        } else {
            Log.d("saving", "save: ");
            editor.putBoolean(key, (Boolean) value);
        }
        editor.commit();
    }

    public String read(String key) {
        Log.d("retreaving", "save: ");
        return this.sharedPreferences.getString(key, "Johndoe");
    }

    public int readInt(String key) {
        Log.d("retreaving", "save: ");
        return this.sharedPreferences.getInt(key, 0);
    }

    public boolean readBoolean(String key) {
        Log.d("retreaving", "save: ");
        return this.sharedPreferences.getBoolean(key, false);
    }


    public boolean logged() {
        if (!this.readBoolean("logged")) {
            return false;
        }
        return true;
    }

    public void logout() {
        editor.clear().commit();
    }

}
