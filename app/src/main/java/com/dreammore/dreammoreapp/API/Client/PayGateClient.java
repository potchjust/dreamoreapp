package com.dreammore.dreammoreapp.API.Client;

import com.dreammore.dreammoreapp.Model.Retrofit.PaymentStatus;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface PayGateClient {

    @POST("status")
    Observable<PaymentStatus> getMobileMoneyStatus(@Query("auth_token")String token, @Query("identifier")String identifier);

}
