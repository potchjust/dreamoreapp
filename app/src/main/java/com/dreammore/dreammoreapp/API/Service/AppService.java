package com.dreammore.dreammoreapp.API.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppService {



     //private static final String BASE_API_URL = "http://192.168.1.152:8000";
     //private static final String BASE_API_URL = "http://192.168.1.113:8000";
     //private static final String BASE_API_URL = "http://192.168.1.72:8000";
     //private static final String BASE_API_URL = "http://192.168.1.135:8000";
     //private static final String BASE_API_URL = "http://192.168.1.100:8000";
     //private static final String BASE_API_URL = "http://192.168.1.68:8000";
     //private static final String BASE_API_URL = "http://dreammoreapp.herokuapp.com";
     //private static final String BASE_API_URL = "http://192.168.1.121:8000";
     //private static final String BASE_API_URL = "http://192.168.43.62:8000";
     //private static final String BASE_API_URL = "http://192.168.1.160:8000/";
     //private static final String BASE_API_URL = "http://192.168.43.62:8000/";
     private static final String PAYGATE_API_URL = "https://paygateglobal.com/api/v2/";
     //private static final String BASE_API_URL = "http://192.168.1.28:8000";
     //private static final String BASE_API_URL = "http://app.dreammore.co/";
     private static final String BASE_API_URL = "http://192.168.1.68:8000/";

     //private static final String BASE_API_URL = "http://10.0.2.2:8000/";

    private static Gson gson=new GsonBuilder()
            .setLenient()
            .create();

    private static Retrofit.Builder reBuilder = new Retrofit.Builder()
            .baseUrl(BASE_API_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson));


    private static Retrofit.Builder reBuilder2=new Retrofit.Builder()
            .baseUrl(PAYGATE_API_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson));

    private static HttpLoggingInterceptor httpLoggingInterceptor =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);


    private static Retrofit retrofit = reBuilder.build();
    private static Retrofit retrofit2 = reBuilder2.build();

    private static OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();


    public static <S> S initService(Class<S> Service) {
        if (!okHttpClient.interceptors().contains(httpLoggingInterceptor)) {
             okHttpClient.addInterceptor(httpLoggingInterceptor)
                         .connectTimeout(5, TimeUnit.MINUTES)
                         .writeTimeout(5, TimeUnit.MINUTES)
                         .readTimeout(5, TimeUnit.MINUTES);
            reBuilder.client(okHttpClient.build());
            retrofit = reBuilder.build();
        }
        return retrofit.create(Service);
    }
    public static <S> S initService2(Class<S> Service) {

        if (!okHttpClient.interceptors().contains(httpLoggingInterceptor)) {
            okHttpClient.addInterceptor(httpLoggingInterceptor)
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES);

            reBuilder2.client(okHttpClient.build());
            retrofit2 = reBuilder2.build();
        }
        return retrofit2.create(Service);
    }
}
