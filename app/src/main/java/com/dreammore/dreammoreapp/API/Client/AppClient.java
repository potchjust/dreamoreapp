package com.dreammore.dreammoreapp.API.Client;

import androidx.lifecycle.MutableLiveData;

import com.dreammore.dreammoreapp.Model.Auth;
import com.dreammore.dreammoreapp.Model.Customer;
import com.dreammore.dreammoreapp.Model.Details;
import com.dreammore.dreammoreapp.Model.Retrofit.PaymentStatus;
import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.Model.RequestResponse;
import com.dreammore.dreammoreapp.Model.Requete;
import com.dreammore.dreammoreapp.Model.Retrofit.Result;
import com.dreammore.dreammoreapp.Model.Retrofit.Payment;
import com.dreammore.dreammoreapp.Model.Retrofit.SlideResult;


import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface AppClient {
    /**
     * register a basic customerq
     *
     * @return void
     */
    @POST("/api/register")
    @FormUrlEncoded
    Call<Auth> register_client(
            @Field("client_fname") String client_fname,
            @Field("client_lname") String client_lname,
            @Field("client_email") String client_email,
            @Field("client_tel") String client_phone,
            @Field("country_code") String country_code
    );

    /*verifier d'abord si un utilisateur avec le même numero existe déja*/
    @GET("api/verify/{telephone}")
    Call<Customer> verify(@Path("telephone") String client_tel);


    @POST("/api/initDraft")
    @FormUrlEncoded
    Call<Draft> send_request(
            @Field("client_id") int client_id,
            @Field("draft_subject") String draft_subject,
            @Field("draft_quantity") int draft_quantity,
            @Field("draft_content") String draft_description,
            @Field("draft_deliver_mode") int deliver_mode

    );

    @POST("/api/initDraft")
    @FormUrlEncoded
    Observable<Draft> sendRequest(
            @Field("client_id") int client_id,
            @Field("draft_subject") String draft_subject,
            @Field("draft_quantity") int draft_quantity,
            @Field("draft_content") String draft_description,
            @Field("draft_deliver_mode") int deliver_mode);

    @GET("/api/myRequest/{customer_id}")
    Call<List<Draft>> getRequest(@Path("customer_id") int id);

    @GET("/api/myRequest/last/{customer_id}")
    Single<List<Draft>> getMyLastRequest(@Path("customer_id") int id);

    @POST("/api/initdraft/sendAttachments")
    @Multipart
    Call<Void> sendAttachments(
            @Part("draft_id") RequestBody draft_id,
            @Part MultipartBody.Part[] attachments
    );

    @POST("/api/initdraft/sendAttachments")
    @Multipart
    Completable sendDraftAttachments(
            @Part("draft_id") RequestBody draft_id,
            @Part MultipartBody.Part attachment
    );

    @GET("/api/myRequest/{client_id}/request/{request_id}/detail")
    Call<Details> getRequestDetails(@Path("client_id") int client_id, @Path("request_id") int request_id);

    @GET("/api/myRequest/{customer_id}/discussion/{request_id}")
    Call<List<Discussion>> getRequestDiscussion(@Path("customer_id") int customer_id, @Path("request_id") int request_id);


    @POST("/api/myRequest/response")
    @FormUrlEncoded
    Call<Result> RespondeToMessage(@Field("customer_id") int customer_id,
                                   @Field("request_id") int request_id,
                                   @Field("admin_id") int admin_id,
                                   @Field("content") String content);

    @POST("/api/user/{id}/saveToken")
    @FormUrlEncoded
    Call<Void> SaveFcmToken(@Path("id") int id, @Field("fcm_token") String token);

    @GET("/api/request/{id}/getInfo")
    Call<RequestResponse> getInfo(@Path("id") int id);

    @GET
    Call<ResponseBody> downloadBill(@Url String url);


    @PATCH("/api/request/{id}/sayOk")
    Call<Draft> validateBill(@Path("id") int id);

    @POST("/api/request/{id}/sayNo")
    @FormUrlEncoded
    Call<Requete> CancelMyBill(@Path("id") int id, @Field("comment") String comment);

    @GET("/api/discussions/Lists/{client_id}")
    Call<List<Discussion>> discussionsList(@Path("client_id") int client_id);

    @POST("/api/update/token/{id}")
    @FormUrlEncoded
    Call<Void> upadeToken(@Path("id") int id, @Field("token") String token);

    @POST("/api/pay/request/init")
    @FormUrlEncoded
        // Call<Purchase>initPayment(@Field("total") double total, @Field("draft_id") int draft_id);
    Observable<Payment> initPayment(@Field("total") double total, @Field("draft_id") int draft_id);

    @POST("/api/request/{purchase_id}/pay")
    @FormUrlEncoded
    Observable<Integer> proceedPayment(@Path("purchase_id") int purchase_id, @Field("paid") int paid);

    @GET("/api/{purchase_id}/checkPay")
    Call<Payment> checkExistingPayment(@Path("purchase_id") int purchase_id);


    @POST("/api/payment/{sub_id}/updatestatus")
    @FormUrlEncoded
    Observable<Integer> updatePaymentStatus(@Path("sub_id") int payment_id,
                                            @Field("paymentStatus") int paymentStatus,
                                            @Field("identifier") String identifier,
                                            @Field("paymentMethod") String paymentMethod);


    @GET("api/loadSlides")
    Call<List<SlideResult>> getSlidesUrl();

}
