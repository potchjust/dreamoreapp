package com.dreammore.dreammoreapp.API.Service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.BuildConfig;
import com.dreammore.dreammoreapp.Helpers.Session;
import com.dreammore.dreammoreapp.Model.Request;
import com.dreammore.dreammoreapp.R;

import com.dreammore.dreammoreapp.View.Activity.HomeActivity;
import com.dreammore.dreammoreapp.View.Activity.SingleItem.SingleActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FireBaseMessagingService  extends FirebaseMessagingService {

    private AppClient appClient=AppService.initService(AppClient.class);
    @Override
    public void onNewToken(String s) {
        updateToken(s);
    }
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String,String> remoteMessageData =remoteMessage.getData();
        int draft_id = Integer.parseInt(remoteMessageData.get("request_id").toString());
        if (remoteMessageData.get("content")!=null){
            showNotification(draft_id,remoteMessageData.get("content"));
        }else{
            showNotification(draft_id,"Veuillez Cliquez pour consulter nos propositions de prix");
        }




    }
    private void showNotification(int id, String message){
        String NOTIFICATION_CHANNEL_ID="com.dreammore.dreammoreapp";

        Intent intent=new Intent(getApplicationContext(),SingleActivity.class);
        intent.putExtra("notif_id",id);
        intent.putExtra("from_notif",true);
        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel=new NotificationChannel(NOTIFICATION_CHANNEL_ID,"com.dreammore.dreammoreapp",NotificationManager.IMPORTANCE_MAX);
            notificationChannel.enableLights(true);
            notificationChannel.setDescription("Request Notification");
            notificationChannel.enableVibration(true);
            notificationChannel.setLightColor(R.color.colorPrimary);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder mBuilder=new NotificationCompat.Builder(getApplicationContext(),NOTIFICATION_CHANNEL_ID);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle("Information")
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentText(message)
                .addAction(R.drawable.ic_notifications,"Ouvrir",pendingIntent)
                .setSmallIcon(R.drawable.dreamore_logo)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(pendingIntent);
        Notification notification=mBuilder.build();
        notificationManager.notify(new Random().nextInt(),notification);

    }
    private void updateToken(String token){
        AppClient appClient=AppService.initService(AppClient.class);
        Session session=new Session(this);
        appClient.upadeToken(session.readInt("client_id"),token).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()){
                    Log.d("token", "onResponse:Update ");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                    t.printStackTrace();
            }
        });
    }

}
