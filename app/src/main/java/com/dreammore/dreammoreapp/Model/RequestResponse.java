package com.dreammore.dreammoreapp.Model;

import com.google.gson.annotations.SerializedName;

public class RequestResponse {

    @SerializedName("attachment_url")
    private String attachment_url;


    public String getAttachment_url() {
        return attachment_url;
    }

    public void setAttachment_url(String attachment_url) {
        this.attachment_url = attachment_url;
    }
}
