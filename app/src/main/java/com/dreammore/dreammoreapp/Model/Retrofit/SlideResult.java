package com.dreammore.dreammoreapp.Model.Retrofit;

import com.google.gson.annotations.SerializedName;

public class SlideResult {

    @SerializedName("id")
    private int id;
    @SerializedName("image_url")
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
