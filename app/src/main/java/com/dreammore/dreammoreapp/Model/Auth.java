package com.dreammore.dreammoreapp.Model;

import com.dreammore.dreammoreapp.Model.Retrofit.Result;
import com.google.gson.annotations.SerializedName;

public class Auth extends Result {

    @SerializedName("client_id")
    private int client_id;

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public int getClient_id() {
        return client_id;
    }

}
