package com.dreammore.dreammoreapp.Model.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class SubPayment {

    @SerializedName("id")
    private int id;
    @SerializedName("paid")
    private double paid;
    @SerializedName("paid_at")
    private Date paid_at;
    @SerializedName("payment_id")
    private int payment_id;

    public int getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(int payment_id) {
        this.payment_id = payment_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public Date getPaid_at() {
        return paid_at;
    }

    public void setPaid_at(Date paid_at) {
        this.paid_at = paid_at;
    }
}
