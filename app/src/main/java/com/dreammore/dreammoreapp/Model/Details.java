package com.dreammore.dreammoreapp.Model;


import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("draft")
    private Draft draft;
    @SerializedName("attachments")
    private Attachments attachments;
    public Details() {
        this.draft=new Draft();
        this.attachments=new Attachments();

    }

    public Draft getDraft() {
        return draft;
    }

    public void setDraft(Draft draft) {
        this.draft = draft;
    }

    public Attachments getAttachments() {
        return attachments;
    }


    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }
}
