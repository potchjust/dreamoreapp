package com.dreammore.dreammoreapp.Model;

import com.google.gson.annotations.SerializedName;

public class Attachments {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("file_extension")
    private String extension;
    @SerializedName("attachment_url")
    private String url;
    @SerializedName("attachable_type")
     private String attachable_type;
    @SerializedName("attachable_id")
    private int attachable_id;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAttachable_type() {
        return attachable_type;
    }

    public void setAttachable_type(String attachable_type) {
        this.attachable_type = attachable_type;
    }

    public int getAttachable_id() {
        return attachable_id;
    }

    public void setAttachable_id(int attachable_id) {
        this.attachable_id = attachable_id;
    }
}
