package com.dreammore.dreammoreapp.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Request {

    @PrimaryKey(autoGenerate = true)
    public int id;
    public int draft_id;
    @ColumnInfo(name = "is_read")
    private int isRead;


    public Request(int draft_id, int isRead) {
        this.draft_id = draft_id;
        this.isRead = isRead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDraft_id() {
        return draft_id;
    }

    public void setDraft_id(int draft_id) {
        this.draft_id = draft_id;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }
}
