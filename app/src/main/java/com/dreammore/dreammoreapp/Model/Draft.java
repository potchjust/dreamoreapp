package com.dreammore.dreammoreapp.Model;

import com.google.gson.annotations.SerializedName;

public class Draft {
    @SerializedName("id")
    private int id ;
    @SerializedName("draft_subject")
    private String draft_subject;
    @SerializedName("received_at")
    private String received_at;
    @SerializedName("draft_deliver_mode")
    private int deliverMode;
    @SerializedName("draft_content")
    private String draftContent;

    @SerializedName("admin_id")
    private int admin_id;
    @SerializedName("client_id")
    private int client_id;
    @SerializedName("draft_quantity")
    private int draft_quantity;

    @SerializedName("state")
    private int statut;

    @SerializedName("to_validate")
    private int validateBy;

    @SerializedName("draft_id")
    private int draft_id;
    @SerializedName("purchase_state")
    private int purchase_state;
    @SerializedName("purchase_id")
    private int purchase_id;
    @SerializedName("total_price")
    private int total_price;
    @SerializedName("attachment_url")
    private String url;

    @SerializedName("adminUser_id")
    private int adminUserId;
    @SerializedName("username")
    private String adminUsername;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }



    public int getValidateBy() {
        return validateBy;
    }

    public void setValidateBy(int validateBy) {
        this.validateBy = validateBy;
    }

    public int getDeliverMode() {
        return deliverMode;
    }


    public void setDeliverMode(int deliverMode) {
        this.deliverMode = deliverMode;
    }

    public String getDraftContent() {
        return draftContent;
    }

    public void setDraftContent(String draftContent) {
        this.draftContent = draftContent;
    }

    public void setDraft_quantity(int draft_quantity) {
        this.draft_quantity = draft_quantity;
    }



    public int getDraft_quantity() {
        return draft_quantity;
    }

      public Draft(){

      }
  public Draft(int id){
    this.draft_quantity=id;
  }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDraft_subject() {
        return draft_subject;
    }

    public void setDraft_subject(String draft_subject) {
        this.draft_subject = draft_subject;
    }

    public String getReceived_at() {
        return received_at;
    }

    public void setReceived_at(String received_at) {
        this.received_at = received_at;
    }

    public int getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }


    public int getStatut(){
        return  this.statut;
    }

    public void setStatut(int statut){
        this.statut=statut;
    }

    public int getDraft_id() {
        return draft_id;
    }

    public void setDraft_id(int draft_id) {
        this.draft_id = draft_id;
    }

    public int getPurchase_state() {
        return purchase_state;
    }

    public void setPurchase_state(int purchase_state) {
        this.purchase_state = purchase_state;
    }

    public int getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(int purchase_id) {
        this.purchase_id = purchase_id;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public int getAdminUserId() {
        if (this.adminUserId==0){
            return 0;
        }else{
            return adminUserId;
        }

    }

    public void setAdminUserId(int adminUserId) {
        this.adminUserId = adminUserId;
    }

    public String getAdminUsername() {

        if(adminUsername==null){
            return "johnDoe";
        }else{
            return adminUsername;
        }

    }

    public void setAdminUsername(String adminUsername) {
        this.adminUsername = adminUsername;
    }
}
