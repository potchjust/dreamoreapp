package com.dreammore.dreammoreapp.Model.Retrofit;

import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("message")
    private String data;
    @SerializedName("code")
    private int code;
    @SerializedName("id_user")
    private int id_user;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("responde")
    private Discussion discussion=new Discussion();

    public Discussion getDiscussion() {
        return discussion;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getCode() {
        return this.code;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
