package com.dreammore.dreammoreapp.Model.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class PaymentStatus {
    @SerializedName("tx_reference")
    private int tx_reference;
    @SerializedName("identifier")
    private String identifier;
    @SerializedName("amount")
    private int amout;
    @SerializedName("payment_reference")
    private String payment_reference;
    @SerializedName("payment_method")
    private String payment_method;
    @SerializedName("datetime")
    private Date datetime;
    @SerializedName("status")
    private int status;
    @SerializedName("error_code")
    private int error_code;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public int getTx_reference() {
        return tx_reference;
    }

    public void setTx_reference(int tx_reference) {
        this.tx_reference = tx_reference;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public int getAmout() {
        return amout;
    }

    public void setAmout(int amout) {
        this.amout = amout;
    }

    public String getPayment_reference() {
        return payment_reference;
    }

    public void setPayment_reference(String payment_reference) {
        this.payment_reference = payment_reference;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
