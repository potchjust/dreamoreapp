package com.dreammore.dreammoreapp.Model.Firebase;

public class ChatUsers {
    private int user_id;
    private int username;

    public ChatUsers() {
    }

    public ChatUsers(int username) {
        this.username = username;
    }

    public ChatUsers(int user_id, int username) {
        this.user_id = user_id;
        this.username = username;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getUsername() {
        return username;
    }

    public void setUsername(int username) {
        this.username = username;
    }
}
