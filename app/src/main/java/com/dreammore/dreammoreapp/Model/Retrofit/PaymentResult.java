package com.dreammore.dreammoreapp.Model.Retrofit;

import com.google.gson.annotations.SerializedName;

public class PaymentResult {
    @SerializedName("subId")
    private int subId;
    @SerializedName("times")
    private int times;

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }
}
