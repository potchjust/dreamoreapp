package com.dreammore.dreammoreapp.Model.Firebase;

public class Messages {
    private int user_id;
    private int admin_id;
    private int request_id;
    private String content;

    public Messages() {
    }

    public Messages(int user_id, int admin_id, int request_id, String content) {
        this.user_id = user_id;
        this.admin_id = admin_id;
        this.request_id = request_id;
        this.content = content;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int request_id) {
        this.request_id = request_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
