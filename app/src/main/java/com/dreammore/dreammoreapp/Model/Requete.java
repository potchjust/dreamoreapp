package com.dreammore.dreammoreapp.Model;

import com.google.gson.annotations.SerializedName;

public class Requete {

    @SerializedName("id")
    private int id;
    @SerializedName("statut")
    private int statut;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }
}
