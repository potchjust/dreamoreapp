package com.dreammore.dreammoreapp.Model.Retrofit;

import com.google.gson.annotations.SerializedName;

public class Payment {
    @SerializedName("id")
    private int id;
    @SerializedName("total_price")
    private int total;
    @SerializedName("draft_id")
    private int draft_id;
    @SerializedName("purchase_state")
    private int purchase_state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getDraft_id() {
        return draft_id;
    }

    public void setDraft_id(int draft_id) {
        this.draft_id = draft_id;
    }


}
