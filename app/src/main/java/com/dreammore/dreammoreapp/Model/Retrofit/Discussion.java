package com.dreammore.dreammoreapp.Model.Retrofit;

import com.google.gson.annotations.SerializedName;

public class Discussion {
    @SerializedName("discuss_at")
    private String discuss_at;
    @SerializedName("content")
    private String content;
    @SerializedName("admin_id")
    private int admin_id;
    @SerializedName("request_id")
    private int request_id;
    @SerializedName("id")
    private int id;
    @SerializedName("from")
    private int from;
    @SerializedName("client_id")
    private int client_id;


    public Discussion(String discuss_at, String content, int admin_id, int request_id, int id, int from, int client_id) {
        this.discuss_at = discuss_at;
        this.content = content;
        this.admin_id = admin_id;
        this.request_id = request_id;
        this.id = id;
        this.from = from;
        this.client_id = client_id;
    }

    public int getClient_id() {
        return client_id;
    }

    public Discussion(){

    }
    public Discussion(String message){
        this.content=message;
    }

    public String getDiscuss_at() {
        return discuss_at;
    }

    public void setDiscuss_at(String discuss_at) {
        this.discuss_at = discuss_at;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public int getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int request_id) {
        this.request_id = request_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}
