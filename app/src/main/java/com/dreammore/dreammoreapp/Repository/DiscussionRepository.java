package com.dreammore.dreammoreapp.Repository;

import androidx.lifecycle.MutableLiveData;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscussionRepository {

    private AppClient appClient;
    private static DiscussionRepository discussionRepository;

    public DiscussionRepository(){
        this.appClient= AppService.initService(AppClient.class);
    }

    public static DiscussionRepository getInstance(){
        if (discussionRepository==null){
            discussionRepository=new DiscussionRepository();
        }
        return discussionRepository;
    }

    public MutableLiveData<List<Discussion>>getDiscussions(int client_id){
        MutableLiveData<List<Discussion>>discussionList=new MutableLiveData<>();
        this.appClient.discussionsList(client_id).enqueue(new Callback<List<Discussion>>() {
            @Override
            public void onResponse(Call<List<Discussion>> call, Response<List<Discussion>> response) {
                if (response.isSuccessful()){
                    if (!response.body().isEmpty()){
                        discussionList.postValue(response.body());
                    }else{
                        discussionList.postValue(null);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Discussion>> call, Throwable t) {
                if(t instanceof IOException){
                    t.printStackTrace();
                }
            }
        });

        return discussionList;
    }
}
