package com.dreammore.dreammoreapp.Repository;

import android.app.ProgressDialog;
import android.net.Uri;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dreammore.dreammoreapp.API.Client.AppClient;
import com.dreammore.dreammoreapp.API.Service.AppService;
import com.dreammore.dreammoreapp.Helpers.Utils;
import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.dreammore.dreammoreapp.Model.Draft;

import com.dreammore.dreammoreapp.Model.Retrofit.SlideResult;
import com.dreammore.dreammoreapp.Model.Retrofit.SubPayment;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Observable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;

public class RequestRepository {

    private static RequestRepository requestRepository;
    private AppClient appClient;
    private boolean isGood;
    private MutableLiveData<List<Draft>> draftList;
    private MutableLiveData<Draft> draft;
    private MutableLiveData<List<SlideResult>>slideResult;

    private MutableLiveData<SubPayment> subPayment;
    private Integer draft_id;
    private ProgressDialog progressDialog;
    private boolean isNetworkIssue;
    private Disposable disposable;
    public LiveData<SubPayment> subPaymentLiveData;

    public RequestRepository() {
        this.appClient = AppService.initService(AppClient.class);
        this.draft_id = 0;
    }

    public static RequestRepository getInstance() {
        if (requestRepository == null) {
            requestRepository = new RequestRepository();
        }
        return requestRepository;
    }

    public MutableLiveData<List<Draft>> getRequest(int client_id) {
        this.draftList = new MutableLiveData<>();

        this.appClient.getRequest(client_id).enqueue(new Callback<List<Draft>>() {
            @Override
            public void onResponse(Call<List<Draft>> call, Response<List<Draft>> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        draftList.setValue(response.body());
                    } else {
                        draftList.setValue(null);
                    }
                }
            }
            @Override
            public void onFailure(Call<List<Draft>> call, Throwable t) {

                if (t instanceof IOException) {
                    isNetworkIssue = true;
                } else {
                    isNetworkIssue = false;
                }
            }
        });
        return draftList;
    }

    /**
     * @param client_id
     * @param subject
     * @param quantity
     * @param content
     * @param deliver_mode
     */
    public MutableLiveData<Draft> SendRequestWithoutAttachments(int client_id, String subject, int quantity, String content, int deliver_mode) {
        draft = new MutableLiveData<>();
        this.appClient.send_request(client_id, subject, quantity, content, deliver_mode).enqueue(new Callback<Draft>() {
            @Override
            public void onResponse(Call<Draft> call, Response<Draft> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.d("hum", "onResponse: " + response.body().getId());
                        draft.postValue(response.body());
                        Log.d("RequestRepositorySend", "onResponse: " + response.code());
                    } else {
                        draft.postValue(response.body());
                        Log.d("RequestRepositorySend", "onResponseNotSend: " + response.code());
                    }
                }
            }

            @Override
            public void onFailure(Call<Draft> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof IOException) {
                    isNetworkIssue = true;
                } else {
                    isNetworkIssue = false;
                }
            }
        });
        return draft;
    }

    public Integer getDraft_id() {
        return draft_id;
    }

    public void setDraft_id(Integer draft_id) {
        this.draft_id = draft_id;
    }

    public boolean isNetworkIssue() {
        return isNetworkIssue;
    }

    public void setNetworkIssue(boolean networkIssue) {
        isNetworkIssue = networkIssue;
    }


    public MutableLiveData<List<Discussion>> getRequestDiscussion(int client_id, int request_id) {
        MutableLiveData<List<Discussion>> data = new MutableLiveData<>();
        this.appClient.getRequestDiscussion(client_id, request_id).enqueue(new Callback<List<Discussion>>() {
            @Override
            public void onResponse(Call<List<Discussion>> call, Response<List<Discussion>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        data.setValue(response.body());
                    } else {
                        data.setValue(null);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Discussion>> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return data;
    }


    /*public MutableLiveData<List<Draft>> getMyLastRequest(int client_id) {
        MutableLiveData<List<Draft>> lastRequest = new MutableLiveData<>();
        this.appClient.getMyLastRequest(client_id).enqueue(new Callback<List<Draft>>() {
            @Override
            public void onResponse(Call<List<Draft>> call, Response<List<Draft>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    lastRequest.postValue(response.body());
                } else {
                    lastRequest.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<List<Draft>> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return lastRequest;
    }*/


    public MutableLiveData<List<Draft>> getMyLastRequest(int client_id){
        MutableLiveData<List<Draft>> listMutableLiveData=new MutableLiveData<>();

        this.appClient.getMyLastRequest(client_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Draft>>() {
                    @Override
                    public void onSuccess(List<Draft> draftList) {
                        if (!draftList.isEmpty()){
                            listMutableLiveData.postValue(draftList);
                        }else{
                            listMutableLiveData.postValue(null);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.getMessage();
                    }
                });
        return  listMutableLiveData;
    }

  /*  private void uplaodFiles(RequestBody draft_id, MultipartBody.Part[] files ){
        this.appClient.sendDraftAttachments(draft_id,files).
    }*/



  private void uploadFile(Uri uri, int id_draft){
        RequestBody draft_id = RequestBody.create(MultipartBody.FORM, String.valueOf(id_draft));
        File file=new File(Utils.getPath(uri));
        RequestBody requestedFile= RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body=MultipartBody.Part.createFormData("attachment",file.getName(),requestedFile);
         appClient.sendDraftAttachments(draft_id,body)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread())
                 .subscribe();

    }


    public MutableLiveData<List<SlideResult>>getSlideUrl(){
        slideResult=new MutableLiveData<>();
        this.appClient.getSlidesUrl().enqueue(new Callback<List<SlideResult>>() {
            @Override
            public void onResponse(Call<List<SlideResult>> call, Response<List<SlideResult>> response) {
                if (response.isSuccessful() && response.body()!=null){
                    slideResult.postValue(response.body());
                }else{
                    slideResult.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<List<SlideResult>> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return  slideResult;
    }








}
