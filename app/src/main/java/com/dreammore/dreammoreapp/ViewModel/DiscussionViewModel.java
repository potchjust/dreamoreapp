package com.dreammore.dreammoreapp.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dreammore.dreammoreapp.Model.Retrofit.Discussion;
import com.dreammore.dreammoreapp.Repository.DiscussionRepository;

import java.util.List;

public class DiscussionViewModel extends ViewModel {

    private DiscussionRepository discussionRepository;

    public DiscussionViewModel(){
        discussionRepository=DiscussionRepository.getInstance();
    }
    public MutableLiveData<List<Discussion>>getDiscussion(int client_id){
        return this.discussionRepository.getDiscussions(client_id);
    }
}
