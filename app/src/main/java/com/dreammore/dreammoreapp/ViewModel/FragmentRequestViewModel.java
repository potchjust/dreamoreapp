package com.dreammore.dreammoreapp.ViewModel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dreammore.dreammoreapp.Model.Draft;
import com.dreammore.dreammoreapp.Model.Retrofit.SlideResult;
import com.dreammore.dreammoreapp.Repository.*;

import java.util.List;

public class FragmentRequestViewModel extends ViewModel {

    private MutableLiveData<List<Draft>> mDraftList = new MutableLiveData<>();

    private RequestRepository requestRepository;
    private Boolean isProcessed;
    private boolean isNetworkError = false;

    public FragmentRequestViewModel() {

        requestRepository = RequestRepository.getInstance();
    }

    public LiveData<List<Draft>> getMyRequests(int client_id) {
        Log.d("ViewModel", "getMyRequests: " + this.requestRepository.isNetworkIssue());

            return this.requestRepository.getRequest(client_id);
    }

    public MutableLiveData<Draft> sendRequest(int client_id, String subject, int quantity, String content, int deliver_mode) {
        return this.requestRepository.SendRequestWithoutAttachments(client_id, subject, quantity, content, deliver_mode);
    }

    public MutableLiveData<List<Draft>>getLastRequest(int client_id){
        return this.requestRepository.getMyLastRequest(client_id);
    }


    public boolean isNetworkError() {
        return isNetworkError;
    }

    public void setNetworkError(boolean networkError) {
        isNetworkError = networkError;
    }


    public MutableLiveData<List<SlideResult>>getSlide(){


        return this.requestRepository.getSlideUrl();
    }


}

