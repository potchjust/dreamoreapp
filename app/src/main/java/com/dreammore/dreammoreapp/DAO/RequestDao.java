package com.dreammore.dreammoreapp.DAO;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dreammore.dreammoreapp.Model.Request;

import java.util.List;

@Dao
public interface RequestDao {

    @Query("select * from Request where draft_id=:id")
    public Request checkIfReaded(int id);

    @Insert
    public void insertRequest(Request request);

    @Query("UPDATE request SET is_read=2 where draft_id= :draft_id")
    public void updateRequest(int draft_id);
}
